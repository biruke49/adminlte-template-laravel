<?php

use App\Exports\ReportExport;
use App\Http\Controllers\ActivityLogController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RejectedDateController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ScheduleRequestController;
use App\Http\Controllers\UserController;
use App\Services\ReportService;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('add.security.headers')->group(function () {


    Route::middleware([
        'auth:sanctum',
        config('jetstream.auth_session'),
        'verified'
    ])->group(function () {

        Route::get('/', function () {

            return redirect()->route('home');
            // $user = Auth::user();

            // if($user->role == 'admin' || $user->role == 'scheduler') return redirect()->route('users');

            // return redirect()->route('requests');
        });

        Route::controller(HomeController::class)->prefix('home')->group(function () {
            Route::get('/', 'index')->name('home');
            Route::get('/equipments/{offset}/{pageNumber}', 'getEquipmentTable');
            Route::get('/cars/{offset}/{pageNumber}', 'getCarTable');
            Route::get('/cameras/{offset}/{pageNumber}', 'getCameraTable');
            Route::get('/schedules/{offset}/{pageNumber}', 'getScheduleTable');
        });


        Route::controller(UserController::class)->prefix('users')->group(function () {
            Route::get('/', 'index')->name('users');
            Route::post('/store', 'store')->name('users.store');
            Route::get('/get-users-table/{offset}/{pageNumber}', 'getUsersTable')->name('users.getTable');
            Route::get('/edit/{user}', 'edit')->name('users.edit');
            Route::post('/update/{user}', 'update')->name('users.update');
            Route::delete('/delete/{user}', 'destroy')->name('users.delete');
            Route::get('/show/{user}', 'show')->name('users.show');
            Route::get('/get-available-hosts', 'getAvailableHosts')->name('users.getAvailableHosts');
            Route::post('/reset-password', 'resetPassword')->name('users.resetPassword');
            Route::post('/change-status/{user}', 'changeStatus')->name('users.changeStatus');
            Route::get('/get-available-camera-man', 'getAvailableCameraMan')->name('users.getAvailableCameraMan');
            Route::post('/update-department/{user}', 'updateDepartment')->name('users.updateDepartment');
        });

        
        Route::controller(ReportController::class)->prefix('reports')->group(function () {
            Route::get('/', 'index')->name('reports');
            Route::post('/filter/{offset}/{pageNumber}', 'filter')->name('reports.filter');
        });


        Route::prefix('/settings')->group(function () {
            Route::get('/activity-logs', [ActivityLogController::class, 'index'])->name('settings.activityLogs');
            // Route::get('/activity-logs/{type}', [ActivityLogController::class, 'show'])->name('settings.logDetails');
            Route::get('/activity-logs/{type}/{offset}/{pageNumber}', [ActivityLogController::class, 'show'])->name('settings.logDetails');
        });
    });


//     Route::get('test', function (ReportService $reportService) {

//         $filters = [
//             "whereTo" => null,
//             "venue" => null,
//             "department" => null,
//             "car" => null,
//             "driver" => null,
//             "requestedBy" => null,
//             "host" => null,
//             "employee" => null,
//             "camera" => null,
//             "cameraMan" => null,
//             "startDate" => null,
//             "endDate" => null,
//             "query" => null,
//         ];

//         $requests = $reportService->filterExcel($filters);
// //    return view('reports.export', ['requests' => $requests]);
//         return Excel::download(new ReportExport($requests), "schedule-requests.xlsx");
//     });


});

// Route::post('/reports/download', 'download')->name('reports.download');
Route::post('/reports/download', [ReportController::class, 'download'])->name('reports.download');
