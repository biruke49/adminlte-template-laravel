<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CameraController;
use App\Http\Controllers\Api\CameraEquipmentController;
use App\Http\Controllers\Api\CarController;
use App\Http\Controllers\Api\ScheduleController;
use App\Http\Controllers\Api\ScheduleRequestController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(AuthController::class)->prefix('auth')->group(function () {
    Route::post('/login', 'login');
    Route::post('/check-user', 'checkUser');
    Route::post('/reset-password', 'resetPassword');
});
Route::middleware('auth:sanctum')->group(function () {

    Route::controller(AuthController::class)->prefix('auth')->group(function(){
        Route::post('/logout', 'logout');
        Route::post('/change-password', 'changePassword');
    });



    Route::controller(UserController::class)->prefix('users')->group(function () {
        // Route::get('/', 'index')->name('users');
        Route::post('/store', 'store')->name('users.store');
        // Route::get('/get-users-table', 'getUsersTable')->name('users.getTable');
        // Route::get('/edit/{user}', 'edit')->name('users.edit');
        Route::put('/update/{user}', 'update')->name('users.update');
        Route::delete('/delete/{user}', 'destroy')->name('users.delete');

        Route::get('/get-available-users/{role}', 'getAvailableUsers')->name('users.getAvailableUsers');

        Route::post('/reset-password', 'resetPassword')->name('users.resetPassword');
        // Route::get('/show/{user}', 'show')->name('users.show');

    });
});


Route::get('fallback/', function () {
    return response()->json([
        'code' => 401,
        'message' => 'Unauthorized!'
    ], 401);
})->name('api-fallback');
