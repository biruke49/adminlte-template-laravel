<?php

namespace Database\Seeders;

use App\Models\Camera;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class CameraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $cameras = [];
        $now = Carbon::now('utc')->toDateTimeString();

        for ($i=0; $i < 3; $i++) {
            $camera = [
                "name"  => $faker->name(),
                "serial_number" => $faker->randomNumber(),
                "status" => "available",
                'created_at' => $now,
                'updated_at' => $now
            ];

            // array_push($cameras, $camera);

            Camera::create($camera);
        }

        // DB::table('cameras')->insert($cameras);
    }
}
