<?php

namespace Database\Seeders;

use App\Models\CameraEquipment;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;


class EquipmentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now('utc')->toDateTimeString();

        $faker = Faker::create();
        $equipments = [];
        for ($i=0; $i < 3; $i++) {
            $equipment = [
                "name"  => $faker->name(),
                "serial_number" => $faker->randomNumber(),
                "status" => "available",
                'created_at' => $now,
                'updated_at' => $now
            ];

            // array_push($equipments, $equipment);
            CameraEquipment::create($equipment);
        }

        // DB::table('camera_equipments')->insert($equipment);
    }
}
