<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        $users = [];
        $roles = ["scheduler", "camera_scheduler", "driver", "camera_man", "host","dispatcher", "employee", "admin"];
        $departments = ["news", "production", "program", "finance", "operational", "other"];
        $now = Carbon::now("utc")->toDateTimeString();

        for ($i=10; $i < 30; $i++) {
            $user = [
                "name" => $faker->name(),
                "email" => "scheduler$i@test.com",
                "phone" => $faker->phoneNumber(),
                "role" => "scheduler",
                "department" => $departments[rand(0,4)],
                "password" => Hash::make("12345678"),
                "created_at" => $now,
                "updated_at" => $now
            ];
            User::create($user);
            $user = [
                "name" => $faker->name(),
                "email" => "employee$i@test.com",
                "phone" => $faker->phoneNumber(),
                "role" => "employee",
                "department" => $departments[rand(3,4)],
                "password" => Hash::make("12345678"),
                "created_at" => $now,
                "updated_at" => $now
            ];
            User::create($user);
            $user = [
                "name" => $faker->name(),
                "email" => $faker->email(),
                "phone" => $faker->phoneNumber(),
                "role" => "host",
                "department" => $departments[rand(0,2)],
                "password" => Hash::make("12345678"),
                "created_at" => $now,
                "updated_at" => $now
            ];
            User::create($user);
            $user = [
                "name" => $faker->name(),
                "email" => "camera_scheduler$i@test.com",
                "phone" => $faker->phoneNumber(),
                "role" => "camera_scheduler",
                "department" => "other",
                "password" => Hash::make("12345678"),
                "created_at" => $now,
                "updated_at" => $now
            ];
            User::create($user);
            $user = [
                "name" => $faker->name(),
                "email" => "driver$i@test.com",
                "phone" => $faker->phoneNumber(),
                "role" => "driver",
                "department" => "other",
                "password" => Hash::make("12345678"),
                "created_at" => $now,
                "updated_at" => $now
            ];
            User::create($user);
            $user = [
                "name" => $faker->name(),
                "email" => "camera_man$i@test.com",
                "phone" => $faker->phoneNumber(),
                "role" => "camera_man",
                "department" => "other",
                "password" => Hash::make("12345678"),
                "created_at" => $now,
                "updated_at" => $now
            ];
            User::create($user);
            $user = [
                "name" => $faker->name(),
                "email" => "dispatcher$i@test.com",
                "phone" => $faker->phoneNumber(),
                "role" => "dispatcher",
                "department" => "other",
                "password" => Hash::make("12345678"),
                "created_at" => $now,
                "updated_at" => $now
            ];
            User::create($user);
            // User::create($user);
            $user = [
                "name" => $faker->name(),
                "email" => "admin$i@test.com",
                "phone" => $faker->phoneNumber(),
                "role" => "admin",
                "department" => "other",
                "password" => Hash::make("12345678"),
                "created_at" => $now,
                "updated_at" => $now
            ];
            User::create($user);
        }
    }
}
