<?php

namespace Database\Seeders;

use App\Models\Car;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $cars = [];
        $now = Carbon::now('utc')->toDateTimeString();

        for ($i=0; $i < 3; $i++) {
            $car = [
                "model"  => $faker->name(),
                "plate_number" => $faker->randomNumber(5, true),
                "color" => "Yellow",
                "code" => $faker->randomNumber(1),
                "status" => "available",
                "km_per_litter" => $faker->numberBetween(15, 50),
                'created_at' => $now,
                'updated_at' => $now
            ];

            // array_push($cars, $car);
            Car::create($car);
        }

        // DB::table('cars')->insert($car);
    }
}
