<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ScheduleRequest>
 */
class ScheduleRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "from" => "2022/09/11 17:00",
            "department" => "news",
            "to" => "2022/09/14 17:00",
            "where_to" => "4 killo",
            "venue" =>  "Addis Ababa University"
        ];
    }
}
