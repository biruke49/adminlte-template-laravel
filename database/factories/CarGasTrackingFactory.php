<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CarGasTracking>
 */
class CarGasTrackingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'car_id' => fake()->text(),
            'driver_id' => fake()->text(),
            'supervisor_id' => fake()->text(),
            'status' => fake()->text(),
            'amount_of_oil' => fake()->text(),
            'previous_distance_covered' => fake()->text(),
            'expected_distance_covered' => fake()->text(),
            'price' => fake()->text(),
            'description' => fake()->text()
        ];
    }
}
