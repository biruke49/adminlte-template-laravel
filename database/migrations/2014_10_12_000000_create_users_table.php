<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('phone_number')->unique();
            $table->string('gender');
            $table->string('role');
            $table->boolean('enabled')->default(true);
            $table->json('address')->nullable();
            $table->foreignId('current_team_id')->nullable();
            $table->string('profile_photo_path', 2048)->nullable();
            $table->text('token')->nullable();
            $table->text('fcm_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'id' => '1',
            'name' => 'Default User',
            'email' => 'ex@mp.le',
            'phone_number' => '0910898682',
            'role' => 'admin',
            'gender' => 'male',
            'enabled' => '1',
            'password' => Hash::make('12345678')
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
