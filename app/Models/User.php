<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable, HasUuid, SoftDeletes;

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'gender',
        'phone_number',
        'enabled',
        'request_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function(Builder $builder){
            $builder->orderBy('users.created_at', 'desc');
        });
    }


    public function schedules(){
        return $this->hasMany(Schedule::class, 'user_id', 'id');
    }

    public function request(){
        return $this->belongsTo(ScheduleRequest::class, 'request_id', 'id');
    }

    public function requests(){
        return $this->belongsToMany(ScheduleRequest::class, 'request_users', 'user_id', 'request_id', 'id', 'id');
    }

    public function canceledSchedules(){
        return $this->hasMany(ScheduleRequest::class, 'canceled_by', 'id');
    }

    public function scopeSearch($query, $search){
        if(!$query) return false;

        return $query->where(function($q) use($search){
            return $q->where('name', 'like', "%$search%")->orWhere('phone_number', 'like', "%$search%");
        });
    }
}
