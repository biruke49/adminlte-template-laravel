<?php

namespace App\Models;

use App\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityLog extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'type_id','type', 'type_name','action','user_id','username','role'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getTypeAttribute($value){
        switch ($value) {
            case 'rejected_dates':
                return "Rejected Date";
            case 'users':
                return "Users";
            default:
                return "Activity Log";
        }
    }

    public function scopeSearch($query, $search){
        if (!$search || $search == "") return $query;
        return $query->Where('username', 'like', "%$search%");
    }


}
