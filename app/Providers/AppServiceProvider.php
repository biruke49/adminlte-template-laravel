<?php

namespace App\Providers;


use App\Repositories\ActivityLog\ActivityLogRepository;
use App\Repositories\ActivityLog\IActivityLogRepository;
use App\Repositories\RejectedDate\IRejectedDateRepository;
use App\Repositories\RejectedDate\RejectedDateRepository;
use App\Repositories\User\IUserRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IUserRepository::class, UserRepository::class);
        $this->app->bind(IActivityLogRepository::class, ActivityLogRepository::class);
        $this->app->bind(IRejectedDateRepository::class, RejectedDateRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
