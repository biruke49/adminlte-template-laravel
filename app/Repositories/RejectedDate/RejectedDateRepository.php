<?php


namespace App\Repositories\RejectedDate;

use App\Models\RejectedDate;
use App\Repositories\Repository;

class RejectedDateRepository extends Repository implements IRejectedDateRepository
{
    public function __construct(RejectedDate $rejectedDate)
    {
        $this->model = $rejectedDate;
        $this->limit = 10;
    }

    public function checkDuplicate($date, $id)
    {
        $existingOffdate = $this->model->where('rejected_date', $date)->where('id', '!=', $id)->first();
        if ($existingOffdate) {
            return true;
        }
        return false;
    }

    public function checkDuplicateForAdd($date)
    {
        $existingOffdate = $this->model->where('rejected_date', $date)->first();
        if ($existingOffdate) {
            return true;
        }
        return false;
    }
    // public function getAll()
    // {
    //     return $this->model->all();
    // }

    // public function getById($id)
    // {
    //     return $this->model->find($id);
    // }

    // public function create(array $attributes)
    // {
    //     return $this->model->create($attributes);
    // }

    // public function update($id, array $attributes)
    // {
    //     return $this->model->where('id',$id)->update($attributes);
    // }

    // public function delete($id)
    // {
    //     return $this->model->where('id',$id)->delete();
    // }

    // public function forceDelete($id)
    // {
    //     return $this->model->where('id',$id)->forceDelete();
    // }

}
