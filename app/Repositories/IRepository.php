<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;

interface IRepository{

    public function getAll(int $offset = 0): Collection;

    public function getById(string $id): mixed;

    public function getMultipleByIds(array $ids): Collection;

        public function create(array $data): mixed;

    public function update(string $id, array $data): bool;

    public function delete(string $id): bool;

    public function forceDelete(string $id): bool;

    public function count(): int;

}
