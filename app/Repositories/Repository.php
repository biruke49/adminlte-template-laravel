<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;

class Repository implements IRepository {

    protected $model;
    protected $limit = 10;


    public function getAll(int $offset = 0): Collection{

        return $this->model->all();
    }

    public function getById(string $id): mixed{
        return $this->model->find($id);
    }

    public function getMultipleByIds(array $ids): Collection{
        return $this->model->whereIn('id', $ids)->get();
    }

    public function create(array $data): mixed{
        return $this->model->create($data);
    }

    public function update(string $id, array $data): bool{
        return $this->getById($id)?->update($data) ?? 0;
    }

    public function delete(string $id): bool{
        return $this->getById($id)?->delete() ?? 0;
    }

    public function forceDelete(string $id): bool{
        return $this->getById($id)?->forceDelete() ?? 0;
    }

    public function count(): int{
        return $this->model->count();
    }
}
