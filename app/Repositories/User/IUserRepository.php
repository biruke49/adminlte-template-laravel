<?php
namespace App\Repositories\User;

use Illuminate\Database\Eloquent\Collection;

interface IUserRepository{

    public function getUsersForSchedule(string $department): Collection;

    public function getByRole(string $role, int $availability = 1): Collection;

    public function checkDuplicate(string $email, string $phone, string $id = null) : bool;

    public function resetPassword(string $userId, string $password): bool;

    public function getUsersForSchedulers(string $department): Collection;

    public function getHosts(string $department, string $role): Collection;

    public function getHostsForProduction(string $role): Collection;

    public function getRole(string $role): Collection;

    public function getDirectors(string $department): Collection;

    public function getCameraSchedulers(): Collection;

    public function getDispatchers(): Collection;
}
