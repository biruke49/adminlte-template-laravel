<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository implements IUserRepository  {

    protected $model;

    public function __construct(User $model){
        $this->model = $model;
    }

    public function getUsersForSchedule(string $department): Collection{
        return $this->model->where('department', $department)->where('role', 'employee')->orWhere('role', 'host')->get();
    }

    public function getByRole(string $role, int $availability = 1): Collection{
        return $role == 'driver' ?
        $this->model->where('role', $role)->get() :
        $this->model->where('role', $role)
        // ->where('status', $availability)
        ->get();
    }

    public function getRole(string $role): Collection{
        return $this->model->where('role', $role)->get();
    }

    public function checkDuplicate(string $email, string $phone, string $id = null) : bool{
        return $id ? (bool) $this->model->where(function ($q) use ($email, $phone){
            return $q->where('email', $email)->orWhere('phone_number', $phone);
        })->where('id', '!=', $id)->first() :
         (bool) $this->model->where(function ($q) use ($email, $phone){
            return $q->where('email', $email)->orWhere('phone_number', $phone);
        })->first();
    }

    public function resetPassword(string $userId, string $password): bool{
        return $this->update($userId, ['password' => Hash::make($password)]);
    }

    public function getUsersForSchedulers(string $department): Collection{
        return $this->model->where('department', $department)->where(fn($q) => $q->where('role', 'host')->orWhere('role', 'employee'))->get();
    }

    public function getHosts(string $department, string $role): Collection{
        return $this->model->where('department', $department)->where('role', $role)
        // ->where('status', 'available')
        ->get();
    }
    public function getHostsForProduction( string $role): Collection{
        return $this->model->where(function($q){
            $q->where('department', 'production')->orWhere('department', 'program');
        })->where('role', $role)
        // ->where('status', 'available')
        ->get();
    }

    public function getDirectors(string $department): Collection{
        return $this->model
        // ->where('department', $department)
        ->where('role', 'director')
        // ->where('status', 'available')
        ->get();
    }

    public function getCameraSchedulers(): Collection
    {
        return  $this->model->where('role', 'camera_scheduler')->get();
    }
    public function getDispatchers(): Collection
    {
        return  $this->model->where('role', 'dispatcher')->get();
    }
}
