<?php

namespace App\Repositories\ActivityLog;

use App\Models\ActivityLog;
use App\Models\User;
use App\Repositories\Repository;

class ActivityLogRepository extends Repository implements IActivityLogRepository  {

    protected $model;

    public function __construct(ActivityLog $model){
        $this->model = $model;
    }
    public function countActivityLog($type){
        return $this->model->where('type', $type)->count();
    }

    public function countByType(){
        return $this->model->selectRaw('type, COUNT(*) as total')->groupBy('type')->get();
    }

    public function getAllActivityLog($type, $offset, $query = null){
        return $this->model->where('type',$type)->orderBy('created_at', 'DESC')->search($query)->limit($this->limit)->offset($offset)->get();
    }

    public function countAllActivityLog($type, $query = null){
        return $this->model->where('type',$type)->orderBy('created_at', 'DESC')->search($query)->count();
    }

    public function countActivityLogType($type,$id){
        return $this->model-> where([['type', $type], ['type_id', $id]])->orWhere('user_id', $id)->count();
    }

    public function search($query, $type, $offset){
        return $this->model->search($query)->type($type)->limit($this->limit)->offset($offset)->get();
    }

    public function countSearched($query, $type){
        return $this->model->search($query)->type($type)->count();
    }




}
