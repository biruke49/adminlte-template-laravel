<?php
namespace App\Repositories\ActivityLog;

interface IActivityLogRepository{

    public function countActivityLog($type);

    public function getAllActivityLog($type, $offset, $query = null);

    public function countAllActivityLog($type, $query = null);

    public function countActivityLogType($type,$id);

    public function countByType();

    public function search($query, $type, $offset);

    public function countSearched($query, $type);

}
