<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AddSecurityHeaders
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request)->header('X-Frame-Options', 'deny')->header('Strict-Transport-Security', 'max-age=31536000;includeSubDomains;preload')->header('X-Content-Type-Options', 'nosniff')->header('X-XSS-Protection', '1; mode=block')->header('Referrer-Policy', 'strict-origin-when-cross-origin')->header('Expect-CT', 'max-age=31536000, enforce')->header('cache-control', 'max-age=0, private, must-revalidate')->header('Permissions-Policy', ' display-capture=(), microphone=(), picture-in-picture=(), screen-wake-lock=*, usb=(), web-share=()')->header('Expect-CT', 'max-age=86400, enforce')->header('Content-Security-Policy', "connect-src 'self'; img-src 'self'; base-uri 'self'; form-action 'self'");
    }
}
