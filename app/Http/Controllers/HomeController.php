<?php

namespace App\Http\Controllers;

use App\Services\CameraService;
use App\Services\CarService;
use App\Services\EquipmentService;
use App\Services\ScheduleRequestService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{

    private $userService;
    private $limit;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        $this->limit = 5;
    }

    public function index()
    {
        try {

            $totalUsers = $this->userService->count();
            // $totalCameras = $this->cameraService->count();
            // $totalEquipments = $this->equipmentService->count();

            // $schedules = $this->requestService->countByStatus();
            // $totalCars = $this->carService->count();

            // $pendingSchedulesCount = $this->requestService->countTodaySchedule();

            // $equipmentCount = $this->equipmentService->countByStatus('on_job');
            // $carCount = $this->carService->countByStatus('on_job');
            // $cameraCount = $this->cameraService->countByStatus('on_job');



            return view(
                'dashboard.dashboard',
                compact(
                    'totalUsers',
                    // 'totalCameras',
                    // 'totalEquipments',
                    // 'schedules',
                    // 'totalCars',
                    // 'pendingSchedulesCount',
                    // 'cameraCount',
                    // 'carCount',
                    // 'equipmentCount'
                )
            );
        } catch (Exception $e) {

            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }

    // public function getEquipmentTable($offset, $pageNumber)
    // {
    //     $limit = $this->limit;
    //     $equipments = $this->equipmentService->getByStatus('on_job', $offset);
    //     $equipmentCount = $this->equipmentService->countByStatus('on_job');

    //     return view('dashboard.equipmentTable', compact('equipmentCount', 'equipments', 'offset', 'limit', 'pageNumber'));
    // }
    // public function getCarTable($offset, $pageNumber)
    // {
    //     $limit = $this->limit;
    //     $cars = $this->carService->getByStatus('on_job', $offset);
    //     $carCount = $this->carService->countByStatus('on_job');

    //     return view('dashboard.carTable', compact('carCount', 'cars', 'offset', 'limit', 'pageNumber'));
    // }
    // public function getCameraTable($offset, $pageNumber)
    // {
    //     $limit = $this->limit;
    //     $cameras = $this->cameraService->getByStatus('on_job', $offset);
    //     $cameraCount = $this->cameraService->countByStatus('on_job');

    //     return view('dashboard.cameraTable', compact('cameraCount', 'cameras', 'offset', 'limit', 'pageNumber'));
    // }
    // public function getScheduleTable($offset, $pageNumber)
    // {
    //     $limit = $this->limit;

    //     $pendingSchedulesCount = $this->requestService->countTodaySchedule();
    //     $pendingSchedules = $this->requestService->getTodaySchedule($offset, $limit);

    //     return view('dashboard.scheduleTable', compact('pendingSchedulesCount', 'pendingSchedules', 'offset', 'limit', 'pageNumber'));
    // }
}
