<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth:sanctum', ['login']);
    // }

    public function login(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required'
            ]);

            if ($validation->fails()) {
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validation->errors()
                ], 400);
            }


            if (!Auth::attempt($request->only(['email', 'password']))) {
                return response()->json([
                    'status' => false,
                    'message' => 'Incorrect Email or Password.',
                ], 401);
            }
            $user = User::where('email', $request->email)->first();

            $token = $user->createToken("API TOKEN")->plainTextToken;
            return response()->json([
                'status' => true,
                'message' => 'User Logged In Successfully',
                'token' => $token,
                'user' => $user
            ], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => false], 500);
        }
    }

    public function logout()
    {
        try {
            auth()->user()->tokens()->delete();
            return response()->json([
                'status' => true,
                'message' => 'User Logged out.',
            ], 200);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => false], 500);
        }
    }

    public function checkUser(){
        $email = request()->validate(['email' => 'required']);
        $user = User::where('email',  $email)->first();

        // return response()->json($user);
        if ($user){
            return response()->json([
                'status' => true,
                'message' => 'Successful'
            ], 200);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'User does not exist',
            ], 400);
        }
    }


    public function resetPassword(){
        $email = request()->validate(['email' => 'required']);
        $newPassword = request()->validate(['new_password' => 'required']);
        $user = User::where('email',  $email)->first();
        $password = $newPassword['new_password'];
        $updatedPassword = Hash::make($password);
        $user->password=$updatedPassword;
        $changed = $user->save();
        if ($changed){
            return response()->json([
                'status' => true,
                'message' => 'Password reset successfully.'
            ], 200);
        }else{
            return response()->json([
                'status' => false,
                'message' => 'User does not exist',
            ], 400);
        }
    }

    public function changePassword(){
        try{
        $oldPassword = request()->validate(['old_password' => 'required']);
        $newPassword = request()->validate(['new_password' => 'required']);
        $confirmPassword = request()->validate(['confirm_password' => 'required']);
        $dbPassword = Auth::user()->password;
        $user = User::where('id',  Auth::user()->id)->first();
        if (Hash::check($oldPassword['old_password'], $dbPassword)) {
            if ($newPassword['new_password'] === $confirmPassword['confirm_password']) {
                $updatedPassword = Hash::make($newPassword['new_password']);
                $user->password=$updatedPassword;
                $changed = $user->save();
                    if ($changed){
                        return response()->json([
                            'status' => true,
                            'message' => 'Your password has been changed successfully'
                        ], 200);
                    }else{
                        return response()->json([
                            'status' => false,
                            'message' => 'User does not exist',
                        ], 400);
                    }
            }else{
                return response()->json([
                    'status' => false,
                    'message' => "Password does not match",
                ], 400);
            }
        }else{
            return response()->json([
                'status' => false,
                'message' => "Incorrect Old Password",
            ], 400);
        }
    }catch (Exception $ex) {
        return response()->json([
            'status' => false,
            'message' => $ex->getMessage(),
        ], 500);
    }
}


}
