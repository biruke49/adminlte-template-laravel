<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ActivityLog;
use App\Models\User;
use App\Services\ActivityLogService;
use App\Services\SmsService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{

    private $smsService, $userService, $activityLogService;

    public function __construct(SmsService $smsService, UserService $userService, ActivityLogService $activityLogService)
    {
        $this->smsService = $smsService;
        $this->userService = $userService;
        $this->activityLogService = $activityLogService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $user = Auth::user();
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'role' => 'required',
                'department' => 'required'
            ]);

            $randStr = Str::random(4);

            if ($this->userService->checkDuplicate($request->email, $request->phone)) {
                $msg = "User already exists.";
                return response()->json(['message' => $msg, 'status' => false], 400);
            }

            $userToBe = [
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'role' => $request->role,
                'department' => $request->department,
                'password' => Hash::make($randStr)
            ];

            $created = $this->userService->create($userToBe);

            if ($created) {
                $activityLog = [
                    'type' => 'users',
                    'type_name' => $user->name,
                    'action' => 'Created a User',
                    'type_id' => $user->id,
                    'user_id' => $user->id,
                    'username' => $user->name,
                    'role' => $user->role,
                    'department' => $user->department,
                ];
                ActivityLog::create($activityLog);
                $msg = "User created successfully!";
                return response()->json(['message' => $msg, 'status' => true, 'data' => $created], 201);
            } else {
                $msg = "Couldn't create a user. Please try again!";
                return response()->json(['message' => $msg, 'status' => false], 400);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => false], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        try {
            $loggedInUser = Auth::user();
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'role' => 'required',
                'department' => 'required'
            ]);
            $update = [
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'role' => $request->role,
                'department' => $request->department,
            ];

            $updated = $this->userService->update($user->id, $update);
            if ($updated) {
                $activityLog = [
                    'type' => 'users',
                    'type_name' => $user->name,
                    'action' => 'Updated a User',
                    'type_id' => $user->id,
                    'user_id' => $loggedInUser->id,
                    'username' => $loggedInUser->name,
                    'role' => $loggedInUser->role,
                    'department' => $loggedInUser->department,
                ];
                ActivityLog::create($activityLog);
                $msg = "User updated successfully!";
                return response()->json(['message' => $msg, 'status' => true], 200);
            } else {
                $msg = "Couldn't update the user. Please try again!";
                return response()->json(['message' => $msg, 'status' => false], 400);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => false], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(User $user)
    {
        try {
            $loggedInUser = Auth::user();

            $deleted = $this->userService->delete($user->id);

            if ($deleted) {
                $activityLog = [
                    'type' => 'users',
                    'type_name' => $user->name,
                    'action' => 'Deleted a User',
                    'type_id' => $user->id,
                    'user_id' => $loggedInUser->id,
                    'username' => $loggedInUser->name,
                    'role' => $loggedInUser->role,
                    'department' => $loggedInUser->department,
                ];
                ActivityLog::create($activityLog);
                $msg = "User deleted successfully!";
                return response()->json(['message' => $msg, 'status' => true], 200);
            } else {
                $msg = "Couldn't delete the User. Please try again!";
                return response()->json(['message' => $msg, 'status' => false], 400);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => false], 500);
        }
    }


    public function getAvailableUsers($role)
    {
        try {
            $loggedInUser = Auth::user();
            $availableRoles = collect(['scheduler', 'camera_scheduler', 'driver', 'camera_man', 'host', 'director', 'dispatcher', 'employee', 'admin']);
            if ($availableRoles->doesntContain($role)) {
                return response()->json(["message" => "The given role does not exist.", "status" => false], 400);
            }

            // if($role == 'host' || $role == "employee"){
            //     if($loggedInUser->role == "admin"){
            //         $users = User::where('status', 'available')->where(fn($q) => $q->where('role', "host")->orWhere('role', 'employee'))->get();
            //         return response()->json(["message" => "available users", "status" => true, "data" => $users], 200);
            //     }elseif($loggedInUser->department == "news" || $loggedInUser->department == "production" || $loggedInUser->department == "program"){
            //         $users = User::where('status', 'available')->where('role', "host")->where('department', $loggedInUser->department)->get();
            //         return response()->json(["message" => "available users", "status" => true, "data" => $users], 200);
            //     }elseif($loggedInUser->department == "operational" || $loggedInUser->department == "finance"){
            //         $users = User::where('status', 'available')->where('role', "employee")->where('department', $loggedInUser->department)->get();
            //         return response()->json(["message" => "available users", "status" => true, "data" => $users], 200);
            //     }else{
            //         return response()->json(['status' => false, 'message' => "You don't have access to this data."], 403);
            //     }
            // }else{

            if($loggedInUser->role == "admin"){
                $users = $role == 'driver' ? User::where('role', $role)->get() : User::where('status', 'available')->where('role', $role)->get();
                return response()->json(["message" => "available users", "status" => true, "data" => $users], 200);
            }else{
                $users = ($loggedInUser->department == 'program') ? ($role == 'driver' ? User::where('role', $role)->get() :  User::where('status', 'available')->where('role', $role)->where( fn($q) => $q->where('department', 'program')->orWhere('department', 'production'))->get()) : ($role == 'driver' ? User::where('role', $role)->get() : User::where('status', 'available')->where('role', $role)->where('department', $loggedInUser->department)->get());
                return response()->json(["message" => "available users", "status" => true, "data" => $users], 200);
            }
            // }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'status' => 500]);
        }
    }

    public function resetPassword(Request $request)
    {
        try {

            $loggedInUser = Auth::user();
            $user = $this->userService->getById($request->user_id);
            $this->validate($request, [
                'password' => 'required',
                'user_id' => 'required',
            ]);


            $updated = $this->userService->resetPassword($request->user_id, $request->password);
            if ($updated) {

                $activityLog = [
                    'type' => 'users',
                    'type_name' => $user->name,
                    'action' => 'Reset password',
                    'type_id' => $user->id,
                    'user_id' => $loggedInUser->id,
                    'username' => $loggedInUser->name,
                    'role' => $loggedInUser->role,
                    'department' => $loggedInUser->department,
                ];
                $this->activityLogService->create($activityLog);
                $msg = "Password Reset Successfully!";
                return response()->json(["message" => $msg, "status" => true], 200);
            } else {
                $msg = "Couldn't reset the password. Please try again!";
                return response()->json(["message" => $msg, "status" => false], 200);
            }
        } catch (Exception $e) {
            return response()->json(["message" => $e->getMessage(), "status" => false, "data" => $e], 500);
        }
    }
}
