<?php

namespace App\Http\Controllers;

use App\Exports\ReportExport;
use App\Services\CameraService;
use App\Services\CarService;
use App\Services\EquipmentService;
use App\Services\ProgramService;
use App\Services\ReportService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    //

    public function __construct(
        private ReportService $reportService,
        private UserService $userService,
        private CameraService $cameraService,
        private EquipmentService $equipmentService,
        private CarService $carService,
        private ProgramService $programService
    ) {
    }

    public function index()
    {

        $user = Auth::user();
        $allowedRoles = collect(['admin', 'viewer']);

        if($allowedRoles->doesntContain($user->role)){
            Session::flash('error', "You don't have access to this data.");
            return back();
        }

        $data['drivers'] = $this->userService->getRole('driver');
        $data['cameraMans'] = $this->userService->getRole('camera_man');
        $data['schedulers'] = $this->userService->getRole('scheduler');
        $data['hosts'] = $this->userService->getRole('host');
        $data['employees'] = $this->userService->getRole('employee');
        $data['admins'] = $this->userService->getRole('admin');

        $data['cameras'] = $this->cameraService->getAll();
        $data['equipments'] = $this->equipmentService->getAll();
        $data['cars'] = $this->carService->getAll();
        $data['programs'] = $this->programService->getAll();

        return view('reports.reports', $data);
    }

    public function filter(Request $request, $offset, $pageNumber)
    {
        $filters = [
            "whereTo" => $request->whereTo ?? null,
            "venue" => $request->venue ?? null,
            "department" => $request->department ?? null,
            "car" => $request->car ?? null,
            "driver" => $request->driver ?? null,
            "requestedBy" => $request->requestedBy ?? null,
            "host" => $request->host ?? null,
            "employee" => $request->employee ?? null,
            "camera" => $request->camera ?? null,
            "cameraMan" => $request->cameraMan ?? null,
            "startDate" => $request->startDate ?? null,
            "endDate" => $request->endDate ?? null,
            "query" => $request->get('query') ?? null,
            "program" => $request->program ?? null,
        ];
        $limit = 10;
        $requests = $this->reportService->filter($filters, $offset);
        $total = $this->reportService->CountFilter($filters);

        return view('reports.reportTable', compact('requests', 'offset', 'total', 'pageNumber', 'limit'));
    }


    public function download(Request $request){
        $filters = [
            "whereTo" => $request->whereTo ?? null,
            "venue" => $request->venue ?? null,
            "department" => $request->department ?? null,
            "car" => $request->car ?? null,
            "driver" => $request->driver ?? null,
            "requestedBy" => $request->requestedBy ?? null,
            "host" => $request->host ?? null,
            "employee" => $request->employee ?? null,
            "camera" => $request->camera ?? null,
            "cameraMan" => $request->cameraMan ?? null,
            "startDate" => $request->start_date ?? null,
            "endDate" => $request->end_date ?? null,
            "query" => $request->get('query') ?? null,
            "program" => $request->program ?? null,

        ];

        $requests = $this->reportService->filterExcel($filters);

        return Excel::download(new ReportExport($requests), "schedule-requests.xlsx");
    }
}
