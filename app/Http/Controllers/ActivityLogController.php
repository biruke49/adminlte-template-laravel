<?php

namespace App\Http\Controllers;

use App\Services\ActivityLogService;
use App\Models\ActivityLog;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class ActivityLogController extends Controller
{
    private $activityLogService;
    private $limit;

    public function __construct(ActivityLogService $activityLogService){
        $this->activityLogService = $activityLogService;
        $this->limit = 10;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try{
            $countedTypes = $this->activityLogService->countByType();
            return view('settings.activityLog', compact('countedTypes'));
        }catch(Exception $e){
            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param ActivityLog $activityLog
     * @return Response
     */
    public function show(Request $request, $type, $offset, $pageNumber)
    {
        try{
            $limit = $this->limit;
            $query = $request->query('searchQuery');
            $activityLogs = $this->activityLogService->getAllActivityLog($type, $offset, $query);
            $total =  $this->activityLogService->countAllActivityLog($type, $query);
            return view('settings.logDetails', compact('activityLogs', 'limit', 'total', 'pageNumber', 'offset'));

        }catch(Exception $e){
            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ActivityLog $activityLog
     * @return Response
     */
    public function edit(ActivityLog $activityLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param ActivityLog $activityLog
     * @return Response
     */
    public function update(Request $request, ActivityLog $activityLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ActivityLog $activityLog
     * @return Response
     */
    public function destroy(ActivityLog $activityLog)
    {
        //
    }
}
