<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Models\ActivityLog;
use App\Models\User;
use App\Services\ActivityLogService;
use App\Services\HahuSmsService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class UserController extends Controller
{

    private $smsService, $userService, $activityLogService;

    public function __construct(
        HahuSmsService     $smsService,
        // SmsService $smsService,
        UserService        $userService,
        ActivityLogService $activityLogService
    ) {
        $this->smsService = $smsService;
        $this->userService = $userService;
        $this->activityLogService = $activityLogService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();
        $allowedRoles = collect(['admin', 'equb_collector']);
        if ($allowedRoles->doesntContain($user->role)) return redirect('/');
        return view('users.users');
    }

    public function getUsersTable(Request $request, $offset, $pageNumber)
    {
        // dd("hello");
        try {
            $limit = 10;
            $user = Auth::user();
            $query = $request->query('searchQuery');

            if ($user->role == 'viewer') {
                $users = User::search($query)->limit($limit)->offset($offset)->get();
                $total = User::search($query)->count();
            } else {
                $users = User::limit($limit)->search($query)->offset($offset)->get();
                $total = User::search($query)->count();
                // $users = $user->role == 'admin' ? User::limit($limit)->search($query)->offset($offset)->get() : User::search($query)->where('role', 'employee')->orWhere('role', 'host')->limit($limit)->offset($offset)->get();
                // $total = $user->role == 'admin' ? User::search($query)->count() : User::where('department', $user->department)->search($query)->where('role', 'employee')->orWhere('role', 'host')->count();
            }
            return view('users.usersTable', compact('users', 'offset', 'limit', 'total', 'pageNumber'));
        } catch (Exception $e) {
            dd("asdfg", $e);
            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {

        $loggedInUser = Auth::user();
        try {
            $validated = $request->validated();
            // $user = Auth::user();
            // $this->validate($request, [
            //     'name' => 'required',
            //     'email' => 'required',
            //     'phone' => 'required',
            //     'role' => 'required',
            //     'gender' => 'required'
            // ]);


            $randStr = fake()->randomNumber(5, true);

            if ($this->userService->checkDuplicate($validated['email'], $validated['phone'])) {
                $msg = "User already exists.";
                $type = 'error';
                Session::flash($type, $msg);
                return redirect()->back();
            }

            $userToBe = [
                'name' => $validated['name'],
                'email' => $validated['email'],
                'phone_number' => $validated['phone'],
                'role' => $validated['role'],
                'gender' => $validated['gender'],
                'password' => Hash::make('12345')
                // 'password' => Hash::make($randStr)
            ];

            $created = $this->userService->create($userToBe);

            if ($created) {
                $message = "A new account has been created on EBS Dispatch for you. You can find your credentials below.\n email - $request->email \n password - $randStr \n role - $request->role";
                $this->smsService->send($message, $validated['phone']);
                $activityLog = [
                    'type' => 'users',
                    'type_name' => $created->name,
                    'action' => 'Created a User',
                    'type_id' => $created->id,
                    'user_id' => $loggedInUser->id,
                    'username' => $loggedInUser->name,
                    'role' => $loggedInUser->role,
                    'gender' => $loggedInUser->gender,
                ];
                ActivityLog::create($activityLog);
                $msg = "User created successfully!";
                $type = 'success';
                Session::flash($type, $msg);
                return redirect()->back();
            } else {
                $msg = "Couldn't create a user. Please try again!";
                $type = 'error';
                Session::flash($type, $msg);
                return redirect()->back();
            }
        } catch (Exception $e) {
            dd($e);
            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(User $user)
    {
        $request = $user->request;

        return view('users.userDetails', compact('request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(User $user)
    {
        return view('users.editUsers', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(User $user)
    {
        try {
            $loggedInUser = Auth::user();
            $deleted = $this->userService->delete($user->id);

            // if ($user->status != 'available') {
            //     $msg = "This User can't be deleted!";
            //     $type = 'error';
            //     Session::flash($type, $msg);
            //     return redirect()->back();
            // }

            if ($deleted) {
                $activityLog = [
                    'type' => 'users',
                    'type_name' => $user->name,
                    'action' => 'Deleted a User',
                    'type_id' => $user->id,
                    'user_id' => $loggedInUser->id,
                    'username' => $loggedInUser->name,
                    'role' => $loggedInUser->role,
                    'gender' => $loggedInUser->gender,
                ];
                ActivityLog::create($activityLog);
                $msg = "User deleted successfully!";
                $type = 'success';
                Session::flash($type, $msg);
                return redirect()->back();
            } else {
                $msg = "Couldn't delete the User. Please try again!";
                $type = 'error';
                Session::flash($type, $msg);
                return redirect()->back();
            }
        } catch (Exception $e) {
            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }

    public function resetPassword(Request $request)
    {
        try {

            $loggedInUser = Auth::user();
            $user = $this->userService->getById($request->user_id);
            $this->validate($request, [
                'password' => 'required',
                'user_id' => 'required',
            ]);


            $updated = $this->userService->resetPassword($request->user_id, $request->password);
            if ($updated) {

                $activityLog = [
                    'type' => 'users',
                    'type_name' => $user->name,
                    'action' => 'Reset password',
                    'type_id' => $user->id,
                    'user_id' => $loggedInUser->id,
                    'username' => $loggedInUser->name,
                    'role' => $loggedInUser->role,
                    'department' => $loggedInUser->department,
                ];
                $this->activityLogService->create($activityLog);
                $msg = "Password Reset Successfully!";
                $type = 'success';
                Session::flash($type, $msg);
                return redirect()->back();
            } else {
                $msg = "Couldn't reset the password. Please try again!";
                $type = 'error';
                Session::flash($type, $msg);
                return redirect()->back();
            }
        } catch (Exception $e) {
            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }

    public function changeStatus(Request $request, User $user)
    {
        try {
            $loggedInUser = Auth::user();
            $roles = collect(["admin", "equb_collector"]);
            $status = collect(["1", "0"]);
            if ($roles->doesntContain($loggedInUser->role)) {
                $msg = "You can't perform this action!";
                $type = 'error';
                Session::flash($type, $msg);
                return back();
            }

            if ($status->doesntContain($request->status)) {
                $msg = "Invalid status!";
                $type = 'error';
                Session::flash($type, $msg);
                return back();
            }

            $updated = $this->userService->update($user->id, ['enabled' => $request->status]);
            if ($updated) {
                $activityLog = [
                    'type' => 'users',
                    'type_name' => $user->name,
                    'action' => "Updated status of the User to $request->status",
                    'type_id' => $user->id,
                    'user_id' => $loggedInUser->id,
                    'username' => $loggedInUser->name,
                    'role' => $loggedInUser->role,
                    'gender' => $loggedInUser->gender,
                ];
                $this->activityLogService->create($activityLog);
                $msg = "Status Updated successfully!";
                $type = 'success';
                Session::flash($type, $msg);
                return redirect()->back();
            } else {
                $msg = "Unable to change the status, Please try again!";
                $type = 'error';
                Session::flash($type, $msg);
                return back();
            }
        } catch (Exception $e) {
            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CreateUserRequest $request, User $user)
    {
        // dd("mmmmm",$user);
        try {
            $loggedInUser = Auth::user();
            $validated = $request->validated();

            if ($this->userService->checkDuplicate($validated['email'], $validated['phone'], $user->id)) {
                $msg = "User already exists.";
                $type = 'error';
                Session::flash($type, $msg);
                return redirect()->back();
            }

            $updated = $this->userService->update($user->id, $validated);

            if ($updated) {

                $activityLog = [
                    'type' => 'users',
                    'type_name' => $user->name,
                    'action' => 'Updated a User',
                    'type_id' => $user->id,
                    'user_id' => $loggedInUser->id,
                    'username' => $loggedInUser->name,
                    'role' => $loggedInUser->role,
                    'department' => $loggedInUser->department,
                ];
                ActivityLog::create($activityLog);
                $msg = "User updated successfully!";
                $type = 'success';
                Session::flash($type, $msg);
                return redirect()->back();
            } else {
                $msg = "Couldn't update the user. Please try again!";
                $type = 'error';
                Session::flash($type, $msg);
                return redirect()->back();
            }
        } catch (Exception $e) {

            $msg = "Unable to process your request, Please try again!";
            $type = 'error';
            Session::flash($type, $msg);
            return back();
        }
    }
}
