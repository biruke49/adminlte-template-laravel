<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportExport implements FromView, ShouldAutoSize
{
    public function __construct(private $requests)
    {

    }

    public function view(): View{
        return view('reports.export', ['requests' => $this->requests]);
    }
}
