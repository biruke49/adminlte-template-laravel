<?php

namespace App\Services;

use App\Repositories\User\IUserRepository;

class UserService {
    private $userRepository, $smsService;

    public function __construct(IUserRepository $userRepository,
     HahuSmsService $smsService
    //  ,SmsService $smsService
     ) {
        $this->userRepository = $userRepository;
        $this->smsService = $smsService;
    }

    public function getById($id) {
        return $this->userRepository->getById($id);
    }

    public function getMultipleById($ids){
        return $this->userRepository->getMultipleByIds($ids);
    }

    public function getAll(){
        return $this->userRepository->getAll();
    }

    public function count(){
        return $this->userRepository->count();
    }

    public function create($equipment){
        return $this->userRepository->create($equipment);
    }

    public function update($id,$attributes){
        return $this->userRepository->update($id, $attributes);
    }

    public function delete($id){
        return $this->userRepository->delete($id);
    }

    public function assignHostToASchedule($hostId, $request){
        return $this->userRepository->update($hostId, ['request_id' => $request->id, 'status' => "on_job"]);
    }

    public function assignDirectorToASchedule($directorId, $request){
        return $this->userRepository->update($directorId, ['request_id' => $request->id, 'status' => "on_job"]);
    }

    public function getAvailableUsers($role, $availability = 1){
        return $this->userRepository->getByRole($role, $availability);
    }

    public function getRole($role){
        return $this->userRepository->getRole($role);
    }

    public function checkDuplicate($email, $phone, $id = null){
        return $this->userRepository->checkDuplicate($email, $phone, $id);
    }

    public function resetPassword($userId, $password){
        return $this->userRepository->resetPassword($userId, $password);
    }

    public function getByRole($role, $availability = 1){
        return $this->userRepository->getByRole($role, $availability);
    }


    public function getHosts($department, $role){
        return $this->userRepository->getHosts($department, $role);
    }

    public function getHostsForProduction($role){
        return $this->userRepository->getHostsForProduction($role);
    }

    public function getUsersForSchedulers($user){
        return $this->userRepository->getUsersForSchedulers($user->department);
    }

    public function sendSms($userId, $message){
        $user = $this->getById($userId);
        return $this->smsService->send($message, $user->phone);
    }

    public function getDirectors($department){
        return $this->userRepository->getDirectors($department);
    }

    public function getCameraSchedulers(){
        return $this->userRepository->getCameraSchedulers();
    }

    public function getDispatchers(){
        return $this->userRepository->getDispatchers();
    }
}
