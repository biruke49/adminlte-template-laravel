<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

define('HAHU_API_KEY', config('keys.HAHU_API_KEY'));
define('HAHU_DEVICE_KEY', config('keys.HAHU_DEVICE_KEY'));

class HahuSmsService {
    private $key;
    private $deviceKey;
    public function __construct()
    {
        $this->key = HAHU_API_KEY;
        $this->deviceKey = HAHU_DEVICE_KEY;
    }

    public function send($message, $to){
         return Http::post("https://hahu.io/api/send/sms?secret=$this->key&mode=devices&phone=$to&message=$message&device=$this->deviceKey&sim=2");
    }

}
