<?php

namespace App\Services;

use Exception;
use Twilio\Rest\Client;

define('TWILIO_ACCOUNT_SID', config('keys.TWILIO_ACCOUNT_SID'));
define('TWILIO_AUTH_TOKEN', config('keys.TWILIO_AUTH_TOKEN'));
define('TWILIO_PHONE_NUMBER', config('keys.TWILIO_PHONE_NUMBER'));
class SmsService
{
    public function send($body, $to)
    {
        try {
            $twilioClient = new Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
            $message = $twilioClient->messages->create($to, ["body" => $body, "from" => TWILIO_PHONE_NUMBER]);
            return $message;
        } catch (Exception $e) {
            return $e;
        }
    }
}
