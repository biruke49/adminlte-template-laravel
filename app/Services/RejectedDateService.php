<?php

namespace App\Services;

use App\Repositories\RejectedDate\IRejectedDateRepository;

class RejectedDateService {
    private $rejectedDateRepository;

    public function __construct(IRejectedDateRepository $rejectedDateRepository) {
        $this->rejectedDateRepository = $rejectedDateRepository;
    }

    public function getById($id) {
        return $this->rejectedDateRepository->getById($id);
    }

    public function getAll(){
        return $this->rejectedDateRepository->getAll();
    }

    public function count(){
        // return $this->rejectedDateRepository->count();
    }

    public function create($equipment){
        return $this->rejectedDateRepository->create($equipment);
    }

    public function update($id,$attributes){
        // dd("asdfghj", $id);
        return $this->rejectedDateRepository->update($id, $attributes);
    }

    public function delete($id){
        return $this->rejectedDateRepository->delete($id);
    }

    public function assignHostToASchedule($hostId, $request){
        return $this->rejectedDateRepository->update($hostId, ['request_id' => $request->id, 'status' => "on_job"]);
    }

    public function assignDirectorToASchedule($directorId, $request){
        return $this->rejectedDateRepository->update($directorId, ['request_id' => $request->id, 'status' => "on_job"]);
    }

    public function getAvailableUsers($role, $availability = 1){
        // return $this->rejectedDateRepository->getByRole($role, $availability);
    }

    public function getRole($role){
        // return $this->rejectedDateRepository->getRole($role);
    }

    public function checkDuplicateForAdd($date){
        return $this->rejectedDateRepository->checkDuplicateForAdd($date);
    }

    public function checkDuplicate($date, $id){
        return $this->rejectedDateRepository->checkDuplicate($date, $id);
    }

    public function resetPassword($userId, $password){
        // return $this->rejectedDateRepository->resetPassword($userId, $password);
    }

    public function getByRole($role, $availability = 1){
        // return $this->rejectedDateRepository->getByRole($role, $availability);
    }


    public function getHosts($department, $role){
        // return $this->rejectedDateRepository->getHosts($department, $role);
    }

    public function getHostsForProduction($role){
        // return $this->rejectedDateRepository->getHostsForProduction($role);
    }

    public function getUsersForSchedulers($user){
        // return $this->rejectedDateRepository->getUsersForSchedulers($user->department);
    }

    public function sendSms($userId, $message){
        // $user = $this->getById($userId);
        // return $this->smsService->send($message, $user->phone);
    }

    public function getDirectors($department){
        // return $this->rejectedDateRepository->getDirectors($department);
    }

    public function getCameraSchedulers(){
        // return $this->rejectedDateRepository->getCameraSchedulers();
    }

    public function getDispatchers(){
        // return $this->rejectedDateRepository->getDispatchers();
    }
}
