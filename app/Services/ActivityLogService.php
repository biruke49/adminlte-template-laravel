<?php

namespace App\Services;

use App\Repositories\ActivityLog\IActivityLogRepository;

class ActivityLogService{
    private $activityLogRepository;

    public function __construct(IActivityLogRepository $activityLogRepository) {
        $this->activityLogRepository = $activityLogRepository;
    }

    public function create($activityLog){
        return $this->activityLogRepository->create($activityLog);
    }


    public function countByType(){
        return $this->activityLogRepository->countByType();
    }

    public function getAllActivityLog($type, $offset = 0, $query = null){
        return $this->activityLogRepository->getAllActivityLog($type, $offset, $query);
    }
    public function countAllActivityLog($type, $query = null){
        return $this->activityLogRepository->countAllActivityLog($type, $query);
    }

    public function countActivityLog($type){
        return $this->activityLogRepository->countActivityLog($type);
    }
}
