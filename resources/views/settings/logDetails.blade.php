<div class="row justify-content-center">
    <p class="card-title">Details on {{ $activityLogs[0]->type ?? 'Activity' }} logs</p>
</div>


<table id="logs-detail" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Action on</th>
            <th>Action By</th>
            <th>Action</th>
            <th>Role</th>
            <th>Date</th>
        </tr>

    </thead>
    <tbody>

        @foreach ($activityLogs as $key => $activityLog)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $activityLog->type_name }}</td>
                <td>{{ $activityLog->username }}</td>
                <td>{{ $activityLog->action }}</td>
                <td>{{ $activityLog?->role }}</td>
                <td>{{ $activityLog?->created_at->diffForHumans() }}</td>
        @endforeach

    </tbody>
</table>



<div class="float-right">
    <nav aria-label="Page navigation" id="paginationDiv">
        <ul class="pagination">

            @if ($offset == 0 || $offset < 0)
                <li class="page-item disabled">
                    <a class="page-link" href="javascript:void(0);" tabindex="-1">
                        <span aria-hidden="true">Previous</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="javascript:void(0);"
                        onclick="logDetails( '{{ $activityLogs[0]->getRawOriginal('type') }}' , {{ $offset - $limit }},{{ $pageNumber - 1 }})"
                        aria-label="Previous">
                        <span aria-hidden="true">Previous</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @endif
            @if ($pageNumber > 3)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="logDetails( '{{ $activityLogs[0]->getRawOriginal('type') }}' , {{ $offset - $limit * 3 }},{{ $pageNumber - 3 }})">{{ $pageNumber - 3 }}</a>
                </li>
            @endif
            @if ($pageNumber > 2)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="logDetails( '{{ $activityLogs[0]->getRawOriginal('type') }}' , {{ $offset - $limit * 2 }},{{ $pageNumber - 2 }})">{{ $pageNumber - 2 }}</a>
                </li>
            @endif
            @if ($pageNumber > 1)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="logDetails( '{{ $activityLogs[0]->getRawOriginal('type') }}' , {{ $offset - $limit }},{{ $pageNumber - 1 }})">{{ $pageNumber - 1 }}</a>
                </li>
            @endif

            <li class="page-item active"> <a class="page-link">{{ $pageNumber }}
                    <span class="sr-only">(current)</span></a></li>

            @if ($offset + $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="logDetails( '{{ $activityLogs[0]->getRawOriginal('type') }}' , {{ $offset + $limit }},{{ $pageNumber + 1 }})">{{ $pageNumber + 1 }}</a>
                </li>
            @endif
            @if ($offset + 2 * $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="logDetails( '{{ $activityLogs[0]->getRawOriginal('type') }}' , {{ $offset + $limit * 2 }},{{ $pageNumber + 2 }})">{{ $pageNumber + 2 }}</a>
                </li>
            @endif
            @if ($offset + 3 * $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="logDetails( '{{ $activityLogs[0]->getRawOriginal('type') }}' , {{ $offset + $limit * 3 }},{{ $pageNumber + 3 }})">{{ $pageNumber + 3 }}</a>
                </li>
            @endif

            @if ($offset + $limit == $total || $offset + $limit > $total)
                <li class="page-item disabled">
                    <a class="page-link" href="javascript:void(0);" tabindex="-1">
                        <span aria-hidden="true">Next</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="javascript:void(0);"
                        onclick="logDetails( '{{ $activityLogs[0]->getRawOriginal('type') }}' , {{ $offset + $limit }},{{ $pageNumber + 1 }})"
                        aria-label="Next">
                        <span aria-hidden="true">Next</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            @endif

        </ul>
    </nav>
</div>

<script type="text/javascript">
    $(function() {
        table = $(`#logs-detail`).DataTable({
            "responsive": true,
            "lengthChange": false,
            "searching": false,
            "autoWidth": false,
            "paginate": false,
            "info": false,
            "buttons": ["colvis"],
            columnDefs: [

                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0,
                },
            ]
        });
        $(`#log-details-colvis-div`).empty();
        table.buttons().container().appendTo(`#log-details-colvis-div`);
        table.on('order.dt search.dt', function() {
            let i = 1;

            table.cells(null, 0, {
                search: 'applied',
                order: 'applied'
            }).every(function(cell) {
                this.data(i++);
            });
        }).draw();

    })
</script>
