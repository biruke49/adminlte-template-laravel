@extends('layouts.main')
@section('styles')
@endsection

@section('content')
    <input type="hidden" name="log_type" id="log_type">

    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item">
                            <a class="nav-link active" id="activity-log-tab" data-toggle="pill" href="#activity-log"
                                role="tab" onclick="removeTables();" aria-controls="employees" aria-selected="true">
                                <span class="fa fa-list"> </span>
                                Activity Logs</a>
                        </li>
                        <li class="nav-item ml-1">
                            <a class="nav-link" id="log-details-tab" style="display: none;" data-toggle="pill"
                                href="#log-details" role="tab" aria-controls="log-details" aria-selected="true">
                                <span class="fa fa-list">
                                </span>
                                Log Details</a>
                        </li>

                    </ul>
                </div>

                <!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content" id="activity-log-data">
                        <div class="tab-pane fade show active" id="activity-log" aria-labelledby="custom-tabs-one-home-tab">
                            <table id="logs" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Type</th>
                                        <th>Action Number</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>



                                    @foreach ($countedTypes as $key => $countedType)
                                        <tr>
                                            <td> {{ $key + 1 }}</td>
                                            <td> On {{ $countedType->type }}</td>
                                            <td>{{ $countedType->total }}</td>
                                            <td><button class="btn btn-secondary"
                                                    onclick="viewLogs('{{ $countedType->getRawOriginal('type') }}', 0, 1)">
                                                    View
                                                    Logs</button></td>
                                        </tr>
                                    @endforeach




                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active d-none" id="log-details"
                            aria-labelledby="custom-tabs-one-home-tab">

                            <div class="float-left searchandClear" id="salary-left-buttons">

                            </div>
                            <div class="float-right">

                                <input type="text" name="searchQuery" id="searchQuery" placeholder="Search"
                                    class="search">
                                <button class="btn btn-default clear" id="clearSearch" onclick="clearSearch();">
                                    Clear
                                </button>
                            </div>
                            <div id="log-details-colvis-div">

                            </div>
                            <div id="log-details-data">

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection


@section('scripts')
    <script>
        $('#nav-u').addClass('active');
        $('#nav-user').addClass('menu-is-opening menu-open active');
        let viewLogs, removeTables, logDetails, clearSearch;
        const loading =
            '<br><div id="loading" class="row d-flex justify-content-center"><div class="row"><img src="' +
            "{{ url('/img/loading.gif') }}" + '"/></div></div>';

        $(function() {
            $('#setting-list').addClass('menu-is-opening menu-open');
            $('#setting-a').addClass('active');
            $('#activity-log-link').addClass('active');

            // $('#log-deatils-tab').addClass('active');
            // $('#activity-log-tab').removeClass('active');

            removeTables = function() {
                $('#activity-log-data').css('display', '');
                $('#log-details-data').css('display', 'none');
                $('#log-details-tab').css('display', 'none');

            }



            let searchField = $('#searchQuery');
            searchField.keydown(function(e) {
                // alert('hi')
                let searchQuery = searchField.val();
                if (e.keyCode == 13) {
                    logDetails($('#log_type').val(), 0, 1, searchQuery);
                }
            });

            clearSearch = function() {
                searchField.val('');
                viewLogs($('#log_type').val(), 0, 1);
            }

            // search = function(query, type, offset, pageNumber) {
            //     if (query == "") {
            //         clearSearch();
            //         return;
            //     }

            //     // let query = $('#searchQuery').val();
            //     $.ajax({
            //         url: `/settings/activity-log/search/${query}/${type}/${offset}/${pageNumber}`,
            //         method: 'get',
            //         success: function(data) {
            //             $('#log-details-data').html(data);
            //             table = $(`#logs-detail`).DataTable({
            //                 "responsive": true,
            //                 "lengthChange": false,
            //                 "searching": false,
            //                 "autoWidth": false,
            //                 "paginate": false,
            //                 "info": false,
            //                 "buttons": ["colvis"],
            //                 columnDefs: [

            //                     {
            //                         "searchable": false,
            //                         "orderable": false,
            //                         "targets": 0,
            //                     },
            //                 ]
            //             });
            //             $(`#log-details-colvis-div`).empty();
            //             table.buttons().container().appendTo(`#log-details-colvis-div`);
            //             table.on('order.dt search.dt', function() {
            //                 let i = 1;

            //                 table.cells(null, 0, {
            //                     search: 'applied',
            //                     order: 'applied'
            //                 }).every(function(cell) {
            //                     this.data(i++);
            //                 });
            //             }).draw();



            //             // myDatatable('employee-table', 1, null);
            //         }
            //     });


            // }


            viewLogs = function(type, offset, pageNumber, query = null) {

                $('#log_type').val(type);
                $('#activity-log-data').css('display', 'none');
                $('#activity-log-tab').removeClass('active');


                $('#log-details-tab').addClass('active');
                $('#log-details-tab').css('display', '');
                $('#log-details').removeClass('d-none');

                $('#log-details-data').css('display', '');
                $('#log-details-data').html(loading);

                if (query) {
                    $.ajax({
                        url: `/settings/activity-logs/${type}/${offset}/${pageNumber}?searchQuery=${query}`,
                        method: 'get',
                        success: function(data) {
                            $('#log-details-data').html(data);
                        }
                    });
                } else {
                    $.ajax({
                        url: `/settings/activity-logs/${type}/${offset}/${pageNumber}`,
                        method: 'get',
                        success: function(data) {
                            $('#log-details-data').html(data);
                        }
                    });
                }



            }

            logDetails = function(type, offset, pageNumber) {
                query = searchField.val();
                $('#log-details-data').html(loading);

                if (query) {
                    $.ajax({
                        url: "/settings/activity-logs/" + type +
                            `/${offset}/${pageNumber}?searchQuery=${query}`,
                        method: 'get',
                        success: function(data) {
                            $('#log-details-data').html(data);
                        }
                    });
                } else {
                    $.ajax({
                        url: "/settings/activity-logs/" + type + `/${offset}/${pageNumber}`,
                        method: 'get',
                        success: function(data) {
                            $('#log-details-data').html(data);
                        }
                    });
                }
            }
            // logDetailsSearch = function(type, offset, pageNumber) {
            //     $('#log-details-data').html(loading);
            //     const query = searchField.val();

            //     $.ajax({
            //         url: `/settings/activity-log/search/${query}/${type}/${offset}/${pageNumber}`,
            //         method: 'get',
            //         success: function(data) {
            //             $('#log-details-data').html(data);
            //         }
            //     });
            // }

            $('#logs').DataTable({
                "responsive": true,
                "lengthChange": false,
                "searching": true,
                "autoWidth": true,
                "buttons": ["colvis"],
            });

        });
    </script>
@endsection
