<table id="user-table" class="table table-bordered table-striped">
    <thead>

        <tr>
            {{-- <th></th> --}}
            <th>No</th>
            <th>Name </th>
            <th>Email</th>
            <th>Phone</th>
            <th>Role</th>
            <th>Gender</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>

    </thead>

    <tbody>

        @foreach ($users as $user)
            @if (auth()->user()->id == $user->id)
                @continue
            @endif

            <tr id="{{ $user->id }}">
                {{-- <td class="details-control" id="user-{{ $user->id }}"></td> --}}
                <td></td>
                <td> {{ $user->name }}</td>
                <td> {{ $user->email }} </td>
                <td> {{ $user->phone_number }} </td>
                @if ($user->role == 'admin')
                    <td> Admin </td>
                @elseif($user->role == 'equb_collector')
                    <td> Equb Collector </td>
                @endif
                @if ($user->gender == 'male')
                    <td> Male </td>
                @elseif($user->gender == 'female')
                    <td> Female </td>
                @endif
                <td>
                    @if ($user->enabled)
                        Active
                    @else
                        Inactive
                    @endif
                </td>

                <td>
                    @if (Auth::user()->role == 'admin')
                        <div class='dropdown'>
                            <button class='btn btn-primary btn-sm btn-flat dropdown-toggle' type='button'
                                data-toggle='dropdown'>Menu<span class='caret'></span></button>
                            <ul class='dropdown-menu dropdown-menu-right p-3'>

                                <li><button class=" btn btn-link" onclick="openEditModal({{ $user }});">
                                        <i class="fas fa-time"></i>Edit</button>
                                </li>
                                <li><button class=" btn btn-link" onclick="openDeleteUserModal({{ $user }});">
                                        <i class="fas fa-bin"></i>Delete</button>
                                </li>
                                <li><button class=" btn btn-link"
                                        onclick="openResetPasswordModal({{ $user }});">
                                        Reset Pass...</button>
                                </li>
                                <li><button class="btn btn-link" onclick="openStatusModal({{ $user }});">
                                        <i class="fas fa-time"></i>Change Status</button>
                                </li>
                                @if ($user->role == 'viewer')
                                    <li><button class="btn btn-link"
                                            onclick="openDepartmentModal({{ $user }});">
                                            <i class="fas fa-time"></i>Update Department</button>
                                    </li>
                                @endif
                                {{-- <li><button class=" btn btn-link" onclick="openScheduleModal({{ $user }});">
                                    <i class="fas fa-time"></i>Schedule</button>
                            </li> --}}
                                {{-- <li><button class=" btn btn-link" onclick="openRequestModal({{ $user }});">
                                    <i class="fas fa-time"></i>Request</button>
                            </li> --}}

                            </ul>
                        </div>
                    @endif
                </td>
            </tr>
        @endforeach

    </tbody>

</table>


<div class="float-right">
    <nav aria-label="Page navigation" id="paginationDiv">
        <ul class="pagination">

            @if ($offset == 0 || $offset < 0)
                <li class="page-item disabled">
                    <a class="page-link" href="javascript:void(0);" tabindex="-1">
                        <span aria-hidden="true">Previous</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="javascript:void(0);"
                        onclick="loadUsers({{ $offset - $limit }},{{ $pageNumber - 1 }})" aria-label="Previous">
                        <span aria-hidden="true">Previous</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @endif
            @if ($pageNumber > 3)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="loadUsers({{ $offset - $limit * 3 }},{{ $pageNumber - 3 }})">{{ $pageNumber - 3 }}</a>
                </li>
            @endif
            @if ($pageNumber > 2)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="loadUsers({{ $offset - $limit * 2 }},{{ $pageNumber - 2 }})">{{ $pageNumber - 2 }}</a>
                </li>
            @endif
            @if ($pageNumber > 1)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="loadUsers({{ $offset - $limit }},{{ $pageNumber - 1 }})">{{ $pageNumber - 1 }}</a>
                </li>
            @endif

            <li class="page-item active"><a class="page-link">{{ $pageNumber }}
                    <span class="sr-only">(current)</span></a></li>

            @if ($offset + $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="loadUsers({{ $offset + $limit }},{{ $pageNumber + 1 }})">{{ $pageNumber + 1 }}</a>
                </li>
            @endif
            @if ($offset + 2 * $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="loadUsers({{ $offset + $limit * 2 }},{{ $pageNumber + 2 }})">{{ $pageNumber + 2 }}</a>
                </li>
            @endif
            @if ($offset + 3 * $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="loadUsers({{ $offset + $limit * 3 }},{{ $pageNumber + 3 }})">{{ $pageNumber + 3 }}</a>
                </li>
            @endif

            @if ($offset + $limit == $total || $offset + $limit > $total)
                <li class="page-item disabled">
                    <a class="page-link" href="javascript:void(0);" tabindex="-1">
                        <span aria-hidden="true">Next</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="javascript:void(0);"
                        onclick="loadUsers({{ $offset + $limit }},{{ $pageNumber + 1 }})" aria-label="Next">
                        <span aria-hidden="true">Next</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            @endif

        </ul>
    </nav>
</div>



<div class="modal" tabindex="-1" role="dialog" id="schedule-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id='schedule-form'>
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-12" style="margin-bottom:20px">
                            <div class="row justify-content-md-center">

                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <span>From <i class="fa fa-asterisk text-danger"
                                                    style="font-size: 8px"></i></span>
                                            <input type="text" class="form-control date-time-picker"
                                                id="from" placeholder="From" autocomplete="0ff" name="from">
                                        </div>
                                        <div class="col-md-6">
                                            <span>To </span>
                                            <input type="text" class="form-control date-time-picker"
                                                id="to" placeholder="To" autocomplete="off" name="to">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="venue">Description <i class="fa fa-asterisk text-danger"
                                                    style="font-size: 8px"></i></label>
                                            <input type="text" class="form-control" id="venue"
                                                placeholder="Description" name="venue">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Schedule</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="change-status-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id='status-form'>
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Change Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12  mt-2">
                            <label for="hosts">Status</label>
                            <select name="status" id="status" style="width: 100%" class="form-control">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="update-department-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post" id='department-form'>
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Change Department</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12  mt-2">
                            <label for="hosts">Department</label>
                            <select name="departments[]" id="change-departments" style="width: 100%"
                                class="form-control multiple-select" multiple>
                                {{-- <option value="other" {{collect($user->department)->contains('other') ? 'selected' : ''}} id="other-option">Other</option> --}}
                            </select>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>

                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="reset-password-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reset Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="reset-password-form">
                @csrf
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <input type="hidden" name="user_id" id="user-id">
                            <div class="col-md-12">
                                <span>Password <i class="fa fa-asterisk text-danger"
                                        style="font-size: 8px"></i></span>
                                <input type="text" class="form-control" id="reset_password"
                                    placeholder="Password" autocomplete="0ff" name="password">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Reset</button>
                </div>

            </form>
        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="delete-user-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>You are about to delete this user</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form action="" method="POST" id="delete-user-form">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-primary">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $('.date-time-picker').datetimepicker({
        format: 'Y/m/d H:i',
        formatTime: 'H:i',
        formatDate: 'Y/m/d',

    });

    $('#change-departments').select2({

    });

    function openScheduleModal(user) {
        $('#schedule-modal').modal('show');
        $('#schedule-form').attr('action', `/requests/store/${user.id}`);
    }

    function openDeleteUserModal(user) {
        $('#delete-user-modal').modal('show');
        $('#delete-user-form').attr('action', '/users/delete/' + user.id);
    }

    function openStatusModal(user) {
        $('#change-status-modal').modal('show');
        $("#status-form").attr('action', `/users/change-status/${user.id}`)
    }

    function openDepartmentModal(user) {
        $('#update-department-modal').modal('show');
        $("#department-form").attr('action', `/users/update-department/${user.id}`);

        let options = '';
        if (user.department.includes('news')) {
            options += `<option value='news' selected>News</option>`;
        } else {
            options += `<option value='news' >News</option>`;
        }
        if (user.department.includes('production')) {
            options += `<option value='production' selected>Production</option>`;
        } else {
            options += `<option value='production' >Production</option>`;
        }
        if (user.department.includes('program')) {
            options += `<option value='program' selected>Program</option>`;
        } else {
            options += `<option value='program' >Program</option>`;
        }
        if (user.department.includes('finance')) {
            options += `<option value='finance' selected>Finance</option>`;
        } else {
            options += `<option value='finance' >Finance</option>`;
        }
        if (user.department.includes('operational')) {
            options += `<option value='operational' selected>Operational</option>`;
        } else {
            options += `<option value='operational' >Operational</option>`;
        }

        // options += `<option value="production" ${) ? 'selected': ''}>Production</option>`;
        // options += `<option value="program" ${user.department.includes('program') ? 'selected': ''}>Program</option>`;
        // options += `<option value="finance" ${user.department.includes('finance') ? 'selected': ''}>Finance</option>`;
        // options += `<option value="operational"${user.department.includes('operational') ? 'selected': ''}>Operational</option>`;

        $('#change-departments').empty();
        $('#change-departments').html(options);

    }

    function openResetPasswordModal(user) {
        $('#reset-password-modal').modal('show');
        $('#reset-password-form').attr('action', '/users/reset-password');
        $('#user-id').val(user.id);
    }

    $('#schedule-form').bootstrapValidator({

        fields: {

            from: {
                validators: {
                    notEmpty: {
                        message: 'Start date is required'
                    },
                }
            },
            to: {
                validators: {
                    notEmpty: {
                        message: 'End date is required'
                    },
                }
            },
            venue: {
                validators: {
                    notEmpty: {
                        message: 'Venue is required'
                    },
                }

            }

        }
    });
</script>
