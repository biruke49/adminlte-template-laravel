@extends('layouts.main')

@section('styles')
    <style>
        .help-block {
            color: #d22630;
        }


        td.details-control {
            background: url("{{ url('img/plus20.png') }}") no-repeat center center;
            cursor: pointer;
        }

        .error {
            text-color: #d22630;
        }

        tr.shown td.details-control {
            background: url("{{ url('img/minus20.jpg') }}") no-repeat center center;
        }

        .dataTables_filter {
            float: right;
        }

        .dataTables_paginate {
            float: right;
        }

        #addAlbumForm fieldset:not(:first-of-type) {
            display: none
        }

        #progressbar {
            overflow: hidden;
            color: lightgrey
        }

        #progressbar .active {
            color: #0275d8
        }

        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            width: 25%;
            float: left;
            position: relative;
            font-weight: 400
        }

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 20px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {
            background: #EBC830
        }

        .progress {
            height: 20px
        }

        .progress-bar {
            background-color: rgb(115, 66, 230)
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }
    </style>
@endsection


@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card ">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item" id='user-nav'>
                            <a class="nav-link active" id="users-tab" data-toggle="pill" href="#users" onclick="removeTabs();"
                                role="tab" aria-controls="users" aria-selected="true"> <span class="fa fa-list"> </span>
                                Users</a>
                        </li>
                        {{-- @if (auth()->user()->role == 'scheduler')
                            <li class="nav-item ml-2" id='request-nav'>
                                <a class="nav-link" id="requests-tab" data-toggle="pill" href="#requests"
                                    onclick="removeTabs();" role="tab" aria-controls="requests" aria-selected="true">
                                    <span class="fa fa-list"> </span>
                                    Requests</a>
                            </li>
                        @endif --}}

                        {{-- <li class="nav-item  ml-2" id='add-nav'>
                            <a class="nav-link " id="add-users-tab" data-toggle="pill" href="#add-users"
                                onclick="removeTabs();" role="tab" aria-controls="users" aria-selected="true"> <span
                                    class="fa fa-list"> </span>
                                Add Users</a>
                        </li>
                        <li class="nav-item  ml-2 d-none" id='edit-nav'>
                            <a class="nav-link " id="edit-users-tab" data-toggle="pill" href="#edit-users" role="tab"
                                aria-controls="users" aria-selected="true"> <span class="fa fa-list"> </span>
                                Edit Users</a>
                        </li> --}}
                    </ul>
                </div>


                <div class="card-body">
                    <div class="container">
                        <div class="tab-content" id="users-tab">
                            <div class="tab-pane show active" id="users">

                                <div class="float-right" id="searchDiv">

                                    {{-- <button type="button" class="btn btn-primary" id="add-request-btn"
                                            onclick="openSmsModal();"
                                            style="margin-right: 10px;"><i class="fab fa-telegram-plane"></i>
                                        Sms
                                    </button> --}}
                                    {{-- <select name="status" id="customer-status">
                                        <option value="all">All</option>
                                        <option value="new">New</option>
                                        <option value="waiting_to_be_assigned">Waiting to be assigned</option>
                                        <option value="contract_signed">Signed Contract</option>
                                        <option value="driver_assigned">Driver Assigned</option>
                                        <option value="disagreed">Disagreed</option>

                                    </select> --}}

                                    @if (Auth::user()->role != 'viewer')
                                        <button type="button" class="btn btn-primary" id="add-user-btn" data-toggle="modal"
                                            data-target="#user-modal" style="margin-right: 30px;"> <span
                                                class="fa fa-plus-circle"></span>
                                            Add</button>
                                    @endif

                                    <input type="text" name="searchQuery" id="searchQuery" placeholder="Search"
                                        class="search">
                                    <button type="button" class="btn btn-default clear" id="clearSearch"
                                        onclick="clearSearch()">
                                        Clear
                                    </button>
                                </div>
                                <div id="users-colvis-div">

                                </div>
                                <div id="users-div">

                                </div>
                            </div>
                            {{-- @if (auth()->user()->role == 'scheduler')
                                <div class="tab-pane show" id="requests">

                                    <div id="requests-div">

                                    </div>
                                </div>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="user-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="post" action="{{ route('users.store') }}" id="add-users-form">
                    <div class="modal-header">
                        <h5 class="modal-title" id="user-modal-title">Add Users</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-sm-12 col-form-label">Name <i
                                    class="fa fa-asterisk text-danger" style="font-size: 8px"></i></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="edit-name" name="name" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-12 col-form-label">Email </label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control" id="edit-email" name="email"
                                    placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-sm-12 col-form-label">Phone <i
                                    class="fa fa-asterisk text-danger" style="font-size: 8px"></i></label>
                            <div class="col-sm-12">
                                <input type="phone" class="form-control" id="edit-phone" name="phone"
                                    placeholder="Phone">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="gender" class="col-sm-12 col-form-label">Gender <i
                                    class="fa fa-asterisk text-danger" style="font-size: 8px"></i></label>
                            <div class="col-sm-12">
                                <select name="gender" class="form-control" id="edit-gender">
                                    <option value=""> Select Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>

                                </select>
                            </div>
                        </div>
                        {{-- @if (auth()->user()->role == 'admin') --}}
                        <div class="form-group row">
                            <label for="role" class="col-sm-12 col-form-label">Role <i
                                    class="fa fa-asterisk text-danger" style="font-size: 8px"></i></label>
                            <div class="col-sm-12">
                                <select name="role" class="form-control" id="edit-role">
                                    <option value=""> Select Role</option>
                                    <option value="admin">Admin</option>
                                    <option value="equb_collector">Equb Collector</option>
                                </select>
                            </div>
                        </div>

                        {{-- @else
                        <div class="form-group row">
                            <label for="role" class="col-sm-12 col-form-label">Role <i
                                    class="fa fa-asterisk text-danger" style="font-size: 8px"></i></label>
                            <div class="col-sm-12">
                                <select name="role" class="form-control" id="edit-role" disabled>
                                    <option value=""> Select Role</option>
                                    <option value="host" {{ auth()->user()->department != 'operational' || auth()->user()->department != 'finance'   ? 'selected' : '' }}>Reporter</option>
                                    <option value="employee" {{ auth()->user()->department == 'operational' || auth()->user()->department == 'finance'   ? 'selected' : '' }}>Finance/Operational</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="department" class="col-sm-12 col-form-label">Department <i
                                    class="fa fa-asterisk text-danger" style="font-size: 8px"></i></label>
                            <div class="col-sm-12">
                                <select name="department" class="form-control" id="edit-department" disabled>
                                    <option value=""> Select Department</option>
                                    <option value="news" {{ auth()->user()->department == 'news' ? 'selected' : '' }}>News</option>
                                    <option value="production" {{ auth()->user()->department == 'production' ? 'selected' : '' }}>Production</option>
                                    <option value="program" {{ auth()->user()->department == 'program' ? 'selected' : '' }}>Program</option>
                                    <option value="finance" {{ auth()->user()->department == 'finance' ? 'selected' : '' }}>Finance</option>
                                    <option value="operational" {{ auth()->user()->department == 'operational' ? 'selected' : '' }}>Operational</option>
                                    <option value="other" id="other-option" {{ auth()->user()->department == 'other' ? 'selected' : '' }}>Other</option>

                                </select>
                            </div>
                        </div>

                        @endif --}}

                        {{-- <div class="form-group row ">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary float-right">Add User</button>
                        </div>
                    </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="user-modal-btn">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#nav-u').addClass('active');
        $('#nav-user').addClass('menu-is-opening menu-open active');

        const loading =
            '<br><div id="loading" class="row d-flex justify-content-center"><div class="row"><img src="' +
            "{{ url('/img/loading.gif') }}" + '"/></div></div>';
        $('#nav-users').addClass('active');
        // $('#users-table')

        let searchField = $('#searchQuery');
        searchField.keydown(function(e) {
            // let searchQuery = searchField.val();
            if (e.keyCode == 13) {
                loadUsers(0, 1);
            }
        });

        let clearSearch = function() {
            if (searchField.val() != "") {
                searchField.val('');
                loadUsers(0, 1);
            }
        }

        function openEditModal(user) {
            console.log("🚀 ~ file: users.blade.php:325 ~ openEditModal ~ user:", user)
            $('#user-modal').modal('show');
            $('#add-users-form').attr('action', `/users/update/${user.id}`);
            $('#user-modal-title').text("Update User");
            $('#edit-name').val(user.name);
            $('#edit-email').val(user.email);
            $('#edit-phone').val(user.phone_number);
            $('#edit-role').val(user.role).change();
            $('#edit-gender').val(user.gender).change();
            $('#user-modal-btn').text("Update User");

        }

        // function openEditTab(user) {
        //     $('#edit-nav').removeClass('d-none');
        //     $('#users-tab').removeClass('active');

        //     $('#edit-users-tab').addClass('active');
        //     $('#users').addClass('d-none');
        //     $('#edit-users').removeClass('d-none');
        //     $('#edit-users').addClass('active');

        //     $.LoadingOverlay("show");
        //     $.ajax({
        //         url: `/users/edit/${user.id}`,
        //         method: 'get',
        //         success: function(data) {
        //             $('#edit-users-div').html(data);
        //             $.LoadingOverlay("hide");
        //         }
        //     });
        // }

        function removeTabs() {
            $('#edit-nav').addClass('d-none');
            $('#users').removeClass('d-none');
            $('#edit-users-tab').removeClass('active');
            $('#edit-users').removeClass('active');

        }

        function loadUsers(offset, pageNumber) {
            $('#users-div').html(loading);
            query = searchField.val();
            $.ajax({
                url: `/users/get-users-table/${offset}/${pageNumber}?searchQuery=${query}`,
                method: 'get',
                success: function(data) {
                    $('#users-div').html(data);

                    let table = $(`#user-table`).DataTable({
                        "responsive": false,
                        "lengthChange": false,
                        "searching": false,
                        "autoWidth": false,
                        "paginate": false,
                        "info": false,
                        "buttons": ["colvis"],
                        'columnDefs': [{
                            'searchable': false,
                            'orderable': false,
                            'target': 1
                        }]
                    });


                    $(`#users-colvis-div`).empty();
                    table.buttons().container().appendTo(`#users-colvis-div`);

                    // table.buttons().container().appendTo(`#user-table_wrapper .col-md-6:eq(0)`);
                    table.on('order.dt search.dt', function() {
                        let i = 1;

                        table.cells(null, 0, {
                            search: 'applied',
                            order: 'applied'
                        }).every(function(cell) {
                            this.data(i++);
                        });
                    }).draw();

                    $('#user-table_filter').prepend(
                        `<button type="button" class=" btn btn-primary" id="register" data-toggle="modal" data-target="#user-modal" style="margin-right: 30px;"> <span class="fa fa-plus-circle"></span> Add</button>`
                    );

                    // $(`#user-table tbody`).on('click', 'td.details-control', function() {
                    //     let tr = $(this).closest('tr');
                    //     tr.addClass('shown');
                    //     let row = table.row(tr);
                    //     let userId = $(this).prop("id").substring(5);
                    //     if (row.child.isShown()) {
                    //         // This row is already open - close it
                    //         row.child.hide();
                    //         tr.removeClass('shown');
                    //     } else {
                    //         // Open this row

                    //         row.child(loading).show();
                    //         $.ajax({
                    //             url: "/users/show/" + userId,
                    //             method: 'get',
                    //             success: function(data) {
                    //                 row.child(data).show();
                    //                 row.child.show();
                    //                 tr.addClass('shown');
                    //                 // myDataTable(`details-request-${requestId}`, 0, 'payment-request-colvis-div')

                    //             }
                    //         });
                    //     }
                    // });

                }
            })
        }

        loadUsers(0, 1);

        // $.ajax({
        //     url: '/requests/get-requests-table',
        //     method: 'get',
        //     success: function(data) {
        //         $('#requests-div').html(data);
        //         myDatatable('request-table', 0, 'add-schedule-modal');
        //     }
        // });

        $('#edit-role').change(function(e) {
            if ($('#edit-role').val() == 'host' && $('#edit-role').val() == 'employee') {
                $('#other-option').prop('disabled', true);
            } else {
                $('#other-option').prop('disabled', false);
            }
        })

        $('#add-users-form').bootstrapValidator({
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z ]+$/,
                            message: 'Name can only consist of alphabets'
                        },
                    }

                },

                email: {

                    validators: {

                        emailAddress: {
                            message: 'Email address is not correct'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'Phone is required'
                        },
                        regexp: {
                            regexp: /^([+][2][5][1]([7]|[9])[0-9]{8}$)|[+][2][5][1][9][0-9]{8}$/,
                            message: 'Phone needs to be in +251.... format'
                        },

                    }

                },
                role: {
                    validators: {
                        notEmpty: {
                            message: 'Role is required'
                        },
                    }

                },
                department: {
                    validators: {
                        notEmpty: {
                            message: 'Department is required'
                        },
                    }

                }

            }
        });
    </script>
@endsection
