<table id="request-table" class="table table-bordered table-striped table-responsive" border="1">
    <thead>
        <tr>
            <th>No</th>
            <th>Program</th>
            <th>Is Internal</th>
            <th>Requested By</th>
            <th>Reporters</th>
            <th>Department</th>
            <th>Schedule</th>
            <th>Description</th>
            <th>Venue</th>
            <th>Dispatcher</th>
            <th>Driver</th>
            <th>Car</th>
            <th>Camera Scheduler</th>
            <th>Camera Man</th>
            <th>Cameras</th>
            <th>Equipments</th>
            <th>Status</th>
        </tr>

    </thead>

    <tbody>

        @foreach ($requests as $key => $request)
            <tr>
                <td> {{$key + 1}}</td>
                <td> {{ $request->is_for_program ? $request->programs()->first()->name : "Unassigned" }} </td>
                <td>{{$request->is_internal ? "✓" : "X"}}</td>
                <td> {{ $request->requestedBy->name ?? 'Unknown' }} </td>
                <td> {{ implode(', ', $request->assigned_users->pluck('name')->toArray()) }}</td>
                <td> {{ strtoupper($request->department) }} </td>
                <td> {{ Carbon\Carbon::parse($request->from)->format('F, d Y h:i a') }}{{ $request->to ? ' to ' . Carbon\Carbon::parse($request->to)->format('F, d Y h:i a') : '' }}</td>
                <td> {{ $request->where_to ?? 'Unassigned' }} </td>
                <td> {{ $request->venue ?? 'Unassigned' }} </td>
                <td> {{ $request->dispatcher->name ?? 'Unassigned' }} </td>
                <td> {{ $request->driver->name ?? 'Unassigned' }} </td>
                <td> {{ $request->car ? $request->car->model . ' to ' . $request->car->plate : 'Unassigned' }} </td>
                <td> {{ $request->cameraScheduler->name ?? 'Unassigned' }} </td>
                <td>{{implode(', ', $request->camera_mans->pluck('name')->toArray())}}</td>
                <td>{{implode(', ', $request->cameras->pluck('name')->toArray())}}</td>
                <td>{{implode(',' , $request->equipments->pluck('name')->toArray())}}</td>
                <td> {{ strtoupper($request->status) }} </td>
            </tr>
        @endforeach
    </tbody>
</table>
