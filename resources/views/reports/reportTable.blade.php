<table id="request-table" class="table table-bordered table-striped table-responsive">
    <thead>

        <tr>
            <th>No</th>
            <th>Program</th>
            <th>Is Internal</th>
            <th>Requested By</th>
            <th>
                @if (auth()->user()->department == 'finance')
                    Finance Staff
                @elseif(auth()->user()->department == 'operational')
                    Operational Staff
                @else
                    Reporters
                @endif
            </th>
            <th>Department</th>
            <th>Schedule</th>
            <th>Description</th>
            <th>Venue</th>
            <th>Dispatcher</th>
            <th>Driver</th>
            <th>Car</th>
            @if (auth()->user()->department != 'finance' ||
                auth()->user()->department != 'operational' ||
                auth()->user()->role == 'admin')
                <th>Camera Scheduler</th>
                <th>Camera Man</th>
                <th>Cameras</th>
                <th>Equipments</th>
            @endif
            <th>Status</th>
        </tr>

    </thead>

    <tbody>

        @foreach ($requests as $key => $request)
            <tr>
                <td></td>
                <td> {{ $request->is_for_program ? $request->programs()->first()->name : "Unassigned" }} </td>
                <td>{{$request->is_internal ? "✓" : "X"}}</td>
                <td> {{ $request->requestedBy->name ?? 'Unknown' }} </td>
                <td>
                    @if (count($request->assigned_users) == 0)
                        Unassigned
                    @else
                        <ul>
                            @foreach ($request->assigned_users as $assignedUser)
                                <li>{{ $assignedUser->name }}</li>
                            @endforeach
                        </ul>
                    @endif
                </td>
                <td> {{ strtoupper($request->department) }} </td>

                <td> {{ Carbon\Carbon::parse($request->from)->format('F, d Y h:i a') }}
                    {{ $request->to ? ' to ' . Carbon\Carbon::parse($request->to)->format('F, d Y h:i a') : '' }}</td>
                <td> {{ $request->where_to ?? 'Unassigned' }} </td>
                <td> {{ $request->venue ?? 'Unassigned' }} </td>
                <td> {{ $request->dispatcher->name ?? 'Unassigned' }} </td>
                <td> {{ $request->driver->name ?? 'Unassigned' }} </td>

                <td> {{ $request->car ? $request->car->model . ' to ' . $request->car->plate : 'Unassigned' }} </td>
                @if (auth()->user()->department != 'finance' ||
                    auth()->user()->department != 'operational' ||
                    auth()->user()->role == 'admin')
                    <td> {{ $request->cameraScheduler->name ?? 'Unassigned' }} </td>
                    <td>
                        @if (count($request->camera_mans) == 0)
                            Unassigned
                        @else
                            <ul>
                                @foreach ($request->camera_mans as $cameraMan)
                                    <li>{{ $cameraMan->name }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </td>
                    <td>
                        @if (count($request->cameras) == 0)
                            Unassigned
                        @else
                            <ul>
                                @foreach ($request->cameras as $camera)
                                    <li>{{ $camera->name }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </td>
                    <td>
                        @if (count($request->equipments) == 0)
                            Unassigned
                        @else
                            <ul>
                                @foreach ($request->equipments as $equipment)
                                    <li>{{ $equipment->name }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </td>
                @endif
                <td> {{ strtoupper($request->status) }} </td>
            </tr>
        @endforeach

    </tbody>
</table>


<div class="float-right">
    <nav aria-label="Page navigation" id="paginationDiv">
        <ul class="pagination">

            @if ($offset == 0 || $offset < 0)
                <li class="page-item disabled">
                    <a class="page-link" href="javascript:void(0);" tabindex="-1">
                        <span aria-hidden="true">Previous</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="javascript:void(0);"
                        onclick="generateReport({{ $offset - $limit }},{{ $pageNumber - 1 }})" aria-label="Previous">
                        <span aria-hidden="true">Previous</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @endif
            @if ($pageNumber > 3)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="generateReport({{ $offset - $limit * 3 }},{{ $pageNumber - 3 }})">{{ $pageNumber - 3 }}</a>
                </li>
            @endif
            @if ($pageNumber > 2)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="generateReport({{ $offset - $limit * 2 }},{{ $pageNumber - 2 }})">{{ $pageNumber - 2 }}</a>
                </li>
            @endif
            @if ($pageNumber > 1)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="generateReport({{ $offset - $limit }},{{ $pageNumber - 1 }})">{{ $pageNumber - 1 }}</a>
                </li>
            @endif

            <li class="page-item active"> <a class="page-link">{{ $pageNumber }}
                    <span class="sr-only">(current)</span></a></li>

            @if ($offset + $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="generateReport({{ $offset + $limit }},{{ $pageNumber + 1 }})">{{ $pageNumber + 1 }}</a>
                </li>
            @endif
            @if ($offset + 2 * $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="generateReport({{ $offset + $limit * 2 }},{{ $pageNumber + 2 }})">{{ $pageNumber + 2 }}</a>
                </li>
            @endif
            @if ($offset + 3 * $limit < $total)
                <li class="page-item"><a class="page-link" href="javascript:void(0);"
                        onclick="generateReport({{ $offset + $limit * 3 }},{{ $pageNumber + 3 }})">{{ $pageNumber + 3 }}</a>
                </li>
            @endif

            @if ($offset + $limit == $total || $offset + $limit > $total)
                <li class="page-item disabled">
                    <a class="page-link" href="javascript:void(0);" tabindex="-1">
                        <span aria-hidden="true">Next</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="javascript:void(0);"
                        onclick="generateReport({{ $offset + $limit }},{{ $pageNumber + 1 }})" aria-label="Next">
                        <span aria-hidden="true">Next</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            @endif

        </ul>
    </nav>
</div>
