@extends('layouts.main')

@section('styles')
    <style>
        .help-block {
            color: #d22630;
        }
    </style>
@endsection


@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card ">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item" id='report-nav'>
                            <a class="nav-link active" id="reports-tab" data-toggle="pill" href="#reports"
                                onclick="removeTabs();" role="tab" aria-controls="reports" aria-selected="true"> <span
                                    class="fa fa-list"> </span>
                                Reports</a>
                        </li>
                    </ul>
                </div>


                <div class="card-body">
                    <div class="container">
                        <div class="tab-content" id="reports-tab">
                            <div class="tab-pane show active" id="reports">
                                <form action="{{ route('reports.download') }}" method="post">
                                    @csrf
                                    <div id="reports-div">
                                        <div class="container">
                                            <div class="row">

                                                <div class="col">
                                                    <input type="text" name="start_date" id="start_date"
                                                        placeholder="Start Date" class="form-control" autocomplete="off" />
                                                </div>
                                                <div class="col">
                                                    <input type="text" name="end_date" id="end_date"
                                                        placeholder="End Date" class="form-control" autocomplete="off" />
                                                </div>
                                                <div class="col">
                                                    <input type="text" name="where_to" id="where_to"
                                                        placeholder="Description" class="form-control" autocomplete="off" />
                                                </div>
                                                <div class="col">
                                                    <input type="text" name="venue" id="venue" placeholder="Venue"
                                                        class="form-control" autocomplete="off" />
                                                </div>

                                            </div>
                                            <div class="row mt-4">
                                                <div class="col">
                                                    <select name="assigned_by" id="assigned_by" class="form-control">
                                                        <option value="all">Select Scheduler</option>
                                                        @foreach ($admins as $admin)
                                                            <option value="{{ $admin->id }}">{{ $admin->name }} -
                                                                {{ $admin->phone }}</option>
                                                        @endforeach
                                                        @foreach ($schedulers as $scheduler)
                                                            <option value="{{ $scheduler->id }}">{{ $scheduler->name }} -
                                                                {{ $scheduler->phone }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <select name="department" id="department" class="form-control">
                                                        @if (Auth::user()->role == 'viewer')
                                                            <option value="">Select Department</option>
                                                        @else
                                                            <option value="all">Select Department</option>
                                                        @endif
                                                        @if (collect(explode(',', Auth::user()->department))->contains('news'))
                                                            <option value="news">News</option>
                                                        @endif
                                                        @if (collect(explode(',', Auth::user()->department))->contains('production'))
                                                            <option value="production">Production</option>
                                                        @endif
                                                        @if (collect(explode(',', Auth::user()->department))->contains('program'))
                                                            <option value="program">Program</option>
                                                        @endif
                                                        @if (collect(explode(',', Auth::user()->department))->contains('finance'))
                                                            <option value="finance">Finance</option>
                                                        @endif
                                                        @if (collect(explode(',', Auth::user()->department))->contains('operational'))
                                                            <option value="operational">Operational</option>
                                                        @endif
                                                    </select>
                                                    <small class="help-block d-none" id="department-error">Department is required</small>
                                                </div>
                                                <div class="col">
                                                    <select name="car" id="car" class="form-control">
                                                        <option value="all">Select Car</option>
                                                        @foreach ($cars as $car)
                                                            <option value="{{ $car->id }}">
                                                                {{ $car->model }}-{{ $car->plate_number }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <select name="driver" id="driver" class="form-control">
                                                        <option value="all">Select Driver</option>
                                                        @foreach ($drivers as $driver)
                                                            <option value="{{ $driver->id }}">{{ $driver->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row mt-4">
                                                <div class="col">
                                                    <select name="host" id="host" class="form-control">
                                                        <option value="all">Select Host</option>
                                                        @foreach ($hosts as $host)
                                                            <option value="{{ $host->id }}">{{ $host->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <select name="employee" id="employee" class="form-control">
                                                        <option value="all">Select Employee</option>
                                                        @foreach ($employees as $employee)
                                                            <option value="{{ $employee->id }}">{{ $employee->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <select name="camera" id="camera" class="form-control">
                                                        <option value="all">Select Camera</option>
                                                        @foreach ($cameras as $camera)
                                                            <option value="{{ $camera->id }}">{{ $camera->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <select name="camera_man" id="camera_man" class="form-control">
                                                        <option value="all">Select Camera Man</option>
                                                        @foreach ($cameraMans as $cameraMan)
                                                            <option value="{{ $cameraMan->id }}">{{ $cameraMan->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>

                                            <div class="row mt-4">
                                                <div class="col-3">
                                                    <select name="program" id="program" class="form-control">
                                                        <option value="all">Select program</option>
                                                        @foreach ($programs as $program)
                                                            <option value="{{ $program->id }}">{{ $program->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="row mt-4">
                                                <div class="col">
                                                    <div class="dropdown form-group justify-content-end">
                                                        <button class="btn btn-primary dropdown-toggle" type="button"
                                                            id="dropdownMenuButton" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                            Generate Report
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <input type="button" onclick="generateReport(0, 1);"
                                                                id="taxBtn" class=" btn btn-primary dropdown-item"
                                                                value="Generate Report">
                                                            <input type="submit" id="downloadBtn"
                                                                class=" btn btn-primary dropdown-item"
                                                                value="Download Excel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                                <div class="card d-none" id="card-table">

                                    <div class="card-header">
                                        <div class="float-right">
                                            <input type="text" name="searchQuery" id="searchQuery"
                                                placeholder="Search" class="search">
                                            <button type="button" class="btn btn-default clear" id="clearSearchD"
                                                onclick="clearSearch()">
                                                Clear
                                            </button>
                                        </div>

                                        <div class="colvis-div">

                                        </div>
                                    </div>

                                    <div class="card-body">

                                        <div id="report-table-data">

                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ url('plugins/downloadjs/download.js') }}"></script>

    <script>
        // $(function() {

        $('#nav-reports').addClass('active');
        const loading =
            '<br><div id="loading" class="row d-flex justify-content-center"><div class="row"><img src="' +
            "{{ url('/img/loading.gif') }}" + '"/></div></div>';


        $('#start_date').datepicker({
            autoclose: true,
            format: 'yyyy-m-d',
        });

        $('#end_date').datepicker({
            autoclose: true,
            endDate: new Date(),
            format: 'yyyy-m-d',

        });

        let searchField = $('#searchQuery');
        searchField.keydown(function(e) {
            let searchQuery = searchField.val();
            if (e.keyCode === 13) {
                generateReport(0, 1);
            }
        });

        let clearSearch = function() {
            if (searchField.val() != "") {
                searchField.val('');
                generateReport(0, 1);
            }
        }

        function generateReport(offset, pageNumber) {

            if($('#department').val() == '' || !$('#department').val()){
                $('#department-error').removeClass('d-none');
                return;
            }
            $('#department-error').addClass('d-none');
            $('#card-table').removeClass('d-none');
            $('#report-table-data').html(loading);
            $.ajax({
                url: `/reports/filter/${offset}/${pageNumber}`,
                method: 'post',
                data: {
                    '_token': "{{ csrf_token() }}",
                    "whereTo": $('#where_to').val(),
                    "venue": $('#venue').val(),
                    "department": $('#department').val(),
                    "car": $('#car').val(),
                    "driver": $('#driver').val(),
                    "requestedBy": $('#assigned_by').val(),
                    "host": $('#host').val(),
                    "employee": $('#employee').val(),
                    "camera": $('#camera').val(),
                    "cameraMan": $('#camera_man').val(),
                    "startDate": $('#start_date').val(),
                    "endDate": $('#end_date').val(),
                    "query": $('#searchQuery').val(),
                    "program": $("#program").val(),
                },
                success: function(data) {

                    $('#report-table-data').html(data);
                    myDatatable('request-table', 0, null, false, false);
                }


            });


        }

        function downloadExcel() {

            generateReport(0, 1);
            $.ajax({
                xhrFields: {
                    responseType: 'blob',
                },
                url: "/reports/download",
                method: 'post',
                data: {
                    '_token': "{{ csrf_token() }}",
                    "whereTo": $('#where_to').val(),
                    "venue": $('#venue').val(),
                    "department": $('#department').val(),
                    "car": $('#car').val(),
                    "driver": $('#driver').val(),
                    "requestedBy": $('#assigned_by').val(),
                    "host": $('#host').val(),
                    "employee": $('#employee').val(),
                    "camera": $('#camera').val(),
                    "cameraMan": $('#camera_man').val(),
                    "startDate": $('#start_date').val(),
                    "endDate": $('#end_date').val(),
                    "query": $('#searchQuery').val(),
                    "program": $("#program").val(),

                },
                success: download.bind(true,
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    `schedule-requests.xlsx`)
            });
        }

        // });
    </script>
@endsection
