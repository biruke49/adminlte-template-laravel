@extends('layouts.main')

@section('styles')
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">

                <div class="row ml-3 mb-1">
                    <h4> <strong>Dashboard </strong> </h4>
                </div>

                <div class="row ml-4">

                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{ $totalUsers }}</h3>

                                <p>Total Users</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-user-tie"></i>
                            </div>
                            {{-- <a href="#" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a> --}}
                        </div>
                    </div>
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                {{-- <h3>{{ $totalCameras }}</h3> --}}
                                <h3>{{ 0 }}</h3>
                                <p>Active Users</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-camera-retro"></i>
                            </div>
                            {{-- <a href="#" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a> --}}
                        </div>
                    </div>
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                {{-- <h3>{{ $totalEquipments }}</h3> --}}
                                <h3>{{ 0 }}</h3>

                                <p>Summary</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-city"></i>
                            </div>
                            {{-- <a href="#" class="small-box-footer">More info <i
                                class="fas fa-arrow-circle-right"></i></a> --}}
                        </div>
                    </div>

                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row ml-4">
                    <div class="col-md-12">
                        <div class="card card-danger">
                            <div class="card-header">
                                <h3 class="card-title">Today's Users</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="chart">
                                    <canvas id="barChart"
                                        style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; width:100%"></canvas>
                                </div>


                            </div>
                            <!-- /.card-body -->
                        </div>

                    </div>
                </div>
                <!-- /.row -->




                <!-- /.row -->
            </div>
        @endsection

        @section('scripts')
            <script>

                $('#nav-dashboard').addClass('active');



                var incomePABarChartCanvas = $('#barChart').get(0).getContext('2d')
                var incomePABarChartData = $.extend(true, {}, incomePAAreaChartData)
                incomePABarChartData.datasets[0] = incomePAAreaChartData.datasets[0];
                // barChartData.datasets[1] =incomePAAreaChartData.datasets[1];

                var incomePABarChartOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    datasetFill: false
                }
                new Chart(incomePABarChartCanvas, {
                    type: 'bar',
                    data: incomePABarChartData,
                    options: incomePABarChartOptions
                })


                // function loadEquipments(offset, pageNumber){
                //     $.ajax({
                //         url: `/home/equipments/${offset}/${pageNumber}`,
                //         method: 'get',
                //         success: function(data){
                //             $("#equipment-div").html(data);
                //         }
                //     });
                // }
                // function loadCars(offset, pageNumber){
                //     $.ajax({
                //         url: `/home/cars/${offset}/${pageNumber}`,
                //         method: 'get',
                //         success: function(data){
                //             $("#car-div").html(data);
                //         }
                //     });
                // }
                // function loadCameras(offset, pageNumber){
                //     $.ajax({
                //         url: `/home/cameras/${offset}/${pageNumber}`,
                //         method: 'get',
                //         success: function(data){
                //             $("#camera-div").html(data);
                //         }
                //     });
                // }

                // function loadSchedules(offset, pageNumber) {
                //     $.ajax({
                //         url: `/home/schedules/${offset}/${pageNumber}`,
                //         method: 'get',
                //         success: function(data){
                //             $('#schedule-div').html(data);
                //         }
                //     });
                // }

                // loadSchedules(0,1);
                // loadEquipments(0, 1);
                // loadCameras(0, 1);
                // loadCars(0, 1);
            </script>
        @endsection
