<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Dashboard</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ url('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ url('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('dist/css/adminlte.min.css') }}">

    <link rel="stylesheet" href="{{ url('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/datetime-picker/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/select2/css/select2.min.css') }}">

    <link rel="shortcut icon" href="{{ url('img/fav.png') }}" type="image/x-icon">

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"
        type="text/css" />

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />

    @yield('styles')
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">

        <!-- Preloader -->
        {{-- <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__wobble" src="{{ url('img/logo2.png') }}" alt="EBS Dispatch Logo"
                height="200" width="200">
        </div> --}}

        @include('partials.navbar')
        @include('partials.sidebar')

        @section('messageSection')
            <section style="margin-left:190px; margin-top:65px;">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-10" id="messages">

                        @if ($errors->any())
                            <div class="alert alert-danger bg-danger text-white">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (Session::get('success'))
                            <div class="alert alert-success alert-block " style="background-color: #00843d;">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ Session::get('success') }}</strong>
                            </div>
                        @elseif(Session::get('error'))
                            <div class="alert alert-error alert-block text-white" style="background-color: #d22630;">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ Session::get('error') }}</strong>
                            </div>
                        @endif

                    </div>
                </div>
            </section>
        @show

        @yield('content')

        @include('partials.footer')

    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ url('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ url('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="{{ url('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ url('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ url('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
    <script src="{{ url('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>
    <!-- ChartJS -->
    <script src="{{ url('plugins/chart.js/Chart.min.js') }}"></script>

    <script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ url('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ url('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ url('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ url('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ url('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ url('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ url('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ url('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ url('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ url('plugins/datetime-picker/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ url('plugins/loadingOverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ url('plugins/select2/js/select2.full.min.js') }}"></script>

    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>


    <!-- AdminLTE for demo purposes -->
    {{-- <script src="dist/js/demo.js"></script> --}}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    {{-- <script src="dist/js/pages/dashboard2.js"></script> --}}
    <script>

        let myDatatable;
        $(function() {

            $('#messages').fadeOut(10000);

             myDatatable = function(tableId, numCol, addBtn = null, colDefs = {}, paginate = true, info = true, searching = false) {
                let table = $(`#${tableId}`).DataTable({
                    "responsive": false,
                    "lengthChange": true,
                    "autoWidth": false,
                    "paginate": paginate,
                    "info": info,
                    "searching": searching,
                    "buttons": ["colvis"],
                    "columnDefs": [{
                        'searchable': false,
                        'orderable': false,
                        'target': numCol
                    }, colDefs]
                });

                table.buttons().container().appendTo(`#${tableId}_wrapper .col-md-6:eq(0)`);

                if (addBtn) {
                    $(`#${tableId}_filter`).prepend(
                        `<button type="button" class=" btn btn-primary" id="register" data-toggle="modal" data-target="#${addBtn}" style="margin-right: 30px;"> <span class="fa fa-plus-circle"></span> Add</button>`
                    );
                }

                table.on('order.dt search.dt', function () {
                    let i = 1;
                    table.cells(null, numCol, {
                        search: 'applied',
                        order: 'applied'
                    }).every(function (cell) {
                        this.data(i++);
                    });
                }).draw();

            }


            // loadRequestCount();

            // setInterval(loadRequestCount, 30000);
        });
    </script>

    @yield('scripts')

</body>

</html>
