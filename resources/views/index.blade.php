@extends('layouts.main')

@section('styles')

@endsection

@section('content')

<div class="content-wrapper">
    <div class="content">
        <div class="card">
            <div class="card-header p-2">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a class="nav-link active" id="requests-tab" data-toggle="pill" href="#requests"
                            role="tab" aria-controls="requests" aria-selected="true"> <span
                                class="fa fa-list"> </span>
                            Requests</a>
                    </li>

                </ul>

            </div>


            <div class="card-body">
                <div class="container">

                    <div class="tab-content" id="requests-tab">

                        <div class="tab-pane show active" id="requests">
                            Hello
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts')

@endsection
